# Project Euler solutions

My solutions to [Project Euler](https://projecteuler.net/) problems.

The solutions are written in C, compiled using Visual Studio Developer Command Prompt with command "cl *filename*.c".

**This repository should not be used to spoil PE problems, and it won't get solutions to problems past #100 as per Project Euler guidelines.**

According to PE,  publishing solutions to problems 1-100 is okay as long as the goal is to discuss and instruct about the problems, not just give answers.
This repository is in "gray area", as I'm not discussing about the problems, but I'm not directly giving out answers either. However, as there are many GitHub/GitLab users with public repositories, I think that this is okay.
