#include <stdio.h>

/*
You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
*/

#define MONDAY      1
#define TUESDAY     2
#define WEDNESDAY   3
#define THURSDAY    4
#define FRIDAY      5
#define SATURDAY    6
#define SUNDAY      7

#define JANUARY     1
#define FEBRUARY    2
#define MARCH       3
#define APRIL       4
#define MAY         5
#define JUNE        6
#define JULY        7
#define AUGUST      8
#define SEPTEMBER   9
#define OCTOBER     10
#define NOVEMBER    11
#define DECEMBER    12

int get_num_days(int month, int year)
{
    switch (month)
    {
        case JANUARY:
        case MARCH:
        case MAY:
        case JULY:
        case AUGUST:
        case OCTOBER:
        case DECEMBER:
        return 31;

        case APRIL:
        case JUNE:
        case SEPTEMBER:
        case NOVEMBER:
        return 30;
    }

    if (month != FEBRUARY)
    { return 0; }

    if (year % 100 == 0)
    { return year % 400 == 0 ? 29 : 28; }

    return year % 4 == 0 ? 29 : 28;
}

int main()
{
    printf("Project Euler 019 - Counting Sundays\n"
            "https://projecteuler.net/problem=19");

    int day     = MONDAY;
    int month   = JANUARY;
    int year    = 1900;

    int answer = 0;

    while (year < 2001)
    {
        int num_days = get_num_days(month, year);

        for (int i = 1; i <= num_days; ++i)
        {
            if (year > 1900)
            {
                if (i == 1 && day == SUNDAY)
                { ++answer; }
            }

            ++day;
            if (day > SUNDAY)
            { day = MONDAY; }
        }

        ++month;
        if (month > DECEMBER)
        {
            ++year;
            month = JANUARY;
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}