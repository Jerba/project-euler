#include <stdio.h>
#include <stdint.h>
#include <math.h>

typedef int32_t int32;
typedef uint64_t uint64;

/*
The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

    d2d3d4=406 is divisible by 2
    d3d4d5=063 is divisible by 3
    d4d5d6=635 is divisible by 5
    d5d6d7=357 is divisible by 7
    d6d7d8=572 is divisible by 11
    d7d8d9=728 is divisible by 13
    d8d9d10=289 is divisible by 17

Find the sum of all 0 to 9 pandigital numbers with this property.
*/

int32 permute(int32 digits[], int32 length);
int32 get_k(int32 arr[], int32 len);
int32 get_l(int32 arr[], int32 len, int32 k);
void swap(int32 *a, int32 *b);
void reverse_array(int32 arr[], int32 len, int32 start);

int main()
{
    printf("Project Euler 043 - Sub-string divisibility\n"
            "https://projecteuler.net/problem=43");

    uint64 primes[] = { 2, 3, 5, 7, 11, 13, 17 };
    int32 digits[]  = { 1, 0, 2, 3, 4, 5, 6, 7, 8, 9 };
    uint64 answer = 0;

    do
    {
        uint64 flag = 1;

        for (int32 i = 0; i < 7; ++i)
        {
            uint64 val = digits[i + 1];
            val = 10 * val + digits[i + 2];
            val = 10 * val + digits[i + 3];

            if (val % primes[i] != 0)
            {
                flag = 0;
                break;
            }
        }

        if (flag)
        {
            uint64 number = digits[0];
            for (int32 i = 1; i < 10; ++i)
            { number = number * 10 + digits[i]; }
            answer += number;
        }
    }
    while (permute(digits, 10) == 0);

    printf("\n\nAnswer: %llu", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 permute(int32 digits[], int32 length)
{
    if (length <= 0) { return 1; }

    int32 k = get_k(digits, length);
    if (k == -1)
    { return 2; }

    int32 l = get_l(digits, length, k);
    if (l == -1)
    { return 3; }

    swap(&digits[k], &digits[l]);
    reverse_array(digits, length, k + 1);

    return 0;
}

int32 get_k(int32 arr[], int32 len)
{
    int32 k = -1;
    if (len <= 0) { return k; }

    for (int32 i = 0; i < len; ++i)
    {
        if (i + 1 < len)
        {
            if (arr[i] < arr[i + 1])
            {
                k = i;
            }
        }
    }

    return k;
}

int32 get_l(int32 arr[], int32 len, int32 k)
{
    int32 l = -1;
    if (len <= 0 || k >= len) { return l; }

    for (int32 i = k + 1; i < len; ++i)
    {
        if (arr[k] < arr[i])
        {
            l = i;
        }
    }

    return l;
}

void swap(int32 *a, int32 *b)
{
    int32 temp = *a;
    *a = *b;
    *b = temp;
}

void reverse_array(int32 arr[], int32 len, int32 start)
{
    if (len <= 0 || start >= len || start < 0) { return; }

    int32 i, j;
    for (i = start, j = 0; j < ((len - start) / 2); ++i, ++j)
    {
        swap(&arr[i], &arr[len - j - 1]);
    }
}
