#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define PRIME_CACHE_SIZE 7920
#define NUM_PRIMES 1000

typedef int32_t int32;
typedef int64_t int64;

/*
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 get_num_digits(int32 number);
int32 are_numbers_permutations_of_one_another(int32 a, int32 b, int32 c);

int32 primes[NUM_PRIMES];
int32 prime_cache[PRIME_CACHE_SIZE];
int32 prime_cache_filled = 0;

int main()
{
    printf("Project Euler 049 - Prime permutations\n"
            "https://projecteuler.net/problem=49");

    fill_prime_cache();

    int32 flag = 0;
    int32 a = 0, b = 0, c = 0;

    for (int32 i = 169; i < NUM_PRIMES - 1; ++i)
    {
        a = primes[i];
        if (a == 1487) { continue; }

        for (int32 j = i + 1; j < NUM_PRIMES; ++j)
        {
            b = primes[j];
            c = b + (b - a);

            if (b >= 10000 || c >= 10000) { break; }
            if (!is_prime(c)) { continue; }

            if (are_numbers_permutations_of_one_another(a, b, c))
            {
                flag = 1;
                goto end;
            }
        }
    }

    end:
    if (!flag)
    { a = b = c = 0; }

    printf("\n\nAnswer: %d%d%d", a, b, c);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }

    prime_cache_filled = 1;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (prime_cache_filled && number < PRIME_CACHE_SIZE) { return prime_cache[number]; }
    if (number == 2 || number == 3) { return 1; }
    if (number % 2 == 0 || number % 3 == 0 || number <= 1) { return 0; }

    int32 i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}

int32 are_numbers_permutations_of_one_another(int32 a, int32 b, int32 c)
{
    a = abs(a);
    b = abs(b);
    c = abs(c);

    if (a == b || a == c || b == c)
    { return 0; }

    int32 num_digits = get_num_digits(a);
    if (num_digits != get_num_digits(b) || num_digits != get_num_digits(c))
    { return 0; }

    int32 digits_a[10] = { 0 };
    int32 digits_b[10] = { 0 };
    int32 digits_c[10] = { 0 };

    for (int32 i = 0; i < num_digits; ++i)
    {
        ++digits_a[a % 10];
        ++digits_b[b % 10];
        ++digits_c[c % 10];
        a /= 10;
        b /= 10;
        c /= 10;
    }

    for (int32 i = 0; i < 10; ++i)
    {
        if (digits_a[i] != digits_b[i] || digits_a[i] != digits_c[i])
        { return 0; }
    }

    return 1;
}
