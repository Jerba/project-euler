#include <stdio.h>

#define TARGET 1000

/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/

int main()
{
    printf("Project Euler 009 - Special Pythagorean triplet\n"
            "https://projecteuler.net/problem=9");

    int a = 0, b = 0, c = 0;
    int answer = 0;

    for (a = 0; a < TARGET / 3; ++a)
    {
        for (b = a + 1; b < TARGET / 2; ++b)
        {
            c = TARGET - a - b;
            if ((a * a) + (b * b) == (c * c))
            {
                answer = a * b * c;
                goto end;
            }
        }
    }

    end:
    printf("\n\nPythagorean triplet: a: %i, b: %i, c: %i", a, b, c);
    printf("\n\nAnswer: %i", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}