#include <stdio.h>

#define GRID_WIDTH 20
#define GRID_HEIGHT 20

/*
Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
 _ _        _ _        _          _                                        
|_|_|  ->      |  ->    |_    ->   |    ->  |_ _   ->  |_     ->  |    
|_|_|          |          |        |_           |        |_       |_ _

How many such routes are there through a 20×20 grid?
*/

int main()
{
    printf("Project Euler 015 - Lattice paths\n"
            "https://projecteuler.net/problem=15");

    unsigned long long answer = 1;

    unsigned long long num_steps = GRID_WIDTH + GRID_HEIGHT;
    unsigned long long choices = GRID_WIDTH > GRID_HEIGHT ? GRID_HEIGHT : GRID_WIDTH;

    for (int i = 0; i < choices; ++i)
    {
        answer = answer * (num_steps - i) / (i + 1);
    }

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");
    getchar();

    return 0;
}