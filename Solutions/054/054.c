#include <stdio.h>
#include <stdint.h>
#include <time.h>

#define BUFFER_SIZE 32
#define NUM_HANDS 1000
#define NUM_CARDS 5
#define WINNER_NONE 0
#define WINNER_PLAYER_ONE 1
#define WINNER_PLAYER_TWO 2

#ifndef bool
#define bool int
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

typedef int32_t int32;

enum hand_rankings_enum
{
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIRS,
    THREE_OF_A_KIND,
    STRAIGHT,
    FLUSH,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    STRAIGHT_FLUSH,
    ROYAL_FLUSH
};

enum card_value_enum
{
    LOW_ACE = 1,
    TWO     = 2,
    THREE   = 3,
    FOUR    = 4,
    FIVE    = 5,
    SIX     = 6,  
    SEVEN   = 7,
    EIGHT   = 8,
    NINE    = 9,
    TEN     = 10,  
    JACK    = 11,
    QUEEN   = 12,
    KING    = 13,
    ACE     = 14
};

enum card_suit_enum
{
    CLUBS       = 0,
    HEARTS      = 1,
    SPADES      = 2,
    DIAMONDS    = 3
};

typedef struct
{
    int32 value;
    int32 suit;
} card_t;

typedef struct
{
    card_t cards[NUM_CARDS];
    int32 sorted_cards[NUM_CARDS];
} hand_t;


void bubble_sort(int32 array[], int32 size);
void swap(int* a, int* b);

int32 read_poker_file();
int32 get_winner(hand_t hand1, hand_t hand2);
int32 get_high_card_winner(hand_t hand1, hand_t hand2);
int32 get_highest_hand_rank(hand_t hand);
int32 get_n_highest_card_value(hand_t hand, int32 n);
int32 get_one_pair_value(hand_t hand);
int32 get_num_pairs(hand_t hand);

bool has_royal_flush(hand_t hand);
bool has_straight_flush(hand_t hand);
bool has_four_of_a_kind(hand_t hand);
bool has_full_house(hand_t hand);
bool has_flush(hand_t hand);
bool has_straight(hand_t hand);
bool has_three_of_a_kind(hand_t hand);
bool has_two_pairs(hand_t hand);
bool has_one_pair(hand_t hand);
bool has_high_card(hand_t hand);

hand_t player_one[NUM_HANDS];
hand_t player_two[NUM_HANDS];

int32 main()
{
    printf("Project Euler 054 - Poker hands\n"
            "https://projecteuler.net/problem=54");

    clock_t start = clock();

    if (read_poker_file() != 0) { return 0; }

    int32 player_one_wins = 0, player_two_wins = 0, draws = 0;
    for (int32 i = 0; i < NUM_HANDS; ++i)
    {
        int32 winner = get_winner(player_one[i], player_two[i]);
        if (winner == WINNER_PLAYER_ONE)
        {
            player_one_wins++;
        }
        else if (winner == WINNER_PLAYER_TWO)
        {
            player_two_wins++;
        }
        else
        {
            draws++;
        }
    }

    clock_t end = clock();
    double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;


    printf("\nPlayer 1 wins: %i, Player 2 wins: %i, Draws: %i", player_one_wins, player_two_wins, draws);
    printf("\n\nAnswer: %i", player_one_wins);

    printf("\n\nProgram took %fmS to execute", (cpu_time_used * 1000.0));
    printf("\nPress Any Key to Quit\n");
    getchar();

    return 0;
}

bool has_royal_flush(hand_t hand)
{
    return hand.sorted_cards[4] == ACE && has_straight_flush(hand);
}

bool has_straight_flush(hand_t hand)
{
    return has_flush(hand) && has_straight(hand);
}

bool has_four_of_a_kind(hand_t hand)
{
    return hand.sorted_cards[0] == hand.sorted_cards[3] || hand.sorted_cards[1] == hand.sorted_cards[4];
}

bool has_full_house(hand_t hand)
{
    return (hand.sorted_cards[0] == hand.sorted_cards[1] && hand.sorted_cards[2] == hand.sorted_cards[4]) ||
        (hand.sorted_cards[0] == hand.sorted_cards[2] && hand.sorted_cards[3] == hand.sorted_cards[4]);
}

bool has_flush(hand_t hand)
{
    int32 suit = hand.cards[0].suit;

    for (int32 i = 1; i < NUM_CARDS; ++i)
    {
        if (hand.cards[i].suit != suit)
        { return false; }
    }

    return true;
}

bool has_straight(hand_t hand)
{
    if (hand.sorted_cards[4] == ACE &&
        hand.sorted_cards[3] == FIVE &&
        hand.sorted_cards[2] == FOUR &&
        hand.sorted_cards[1] == THREE &&
        hand.sorted_cards[0] == TWO)
    {
        return true;
    }

    int32 temp = hand.sorted_cards[0];
    for (int32 i = 1; i < NUM_CARDS; ++i)
    {
        if (hand.sorted_cards[i] != temp + 1)
        { return false; }
        temp = hand.sorted_cards[i];
    }

    return true;
}

bool has_three_of_a_kind(hand_t hand)
{
    return hand.sorted_cards[0] == hand.sorted_cards[2] ||
            hand.sorted_cards[2] == hand.sorted_cards[4] ||
            hand.sorted_cards[1] == hand.sorted_cards[3];
}

bool has_two_pairs(hand_t hand)
{
    return get_num_pairs(hand) == 2;
}

bool has_one_pair(hand_t hand)
{
    return get_num_pairs(hand) == 1;
}

int32 get_winner(hand_t hand1, hand_t hand2)
{
    int32 hand1_rank = get_highest_hand_rank(hand1);
    int32 hand2_rank = get_highest_hand_rank(hand2);

    if (hand1_rank > hand2_rank) { return WINNER_PLAYER_ONE; }
    if (hand2_rank > hand1_rank) { return WINNER_PLAYER_TWO; }

    switch (hand1_rank)
    {
        case HIGH_CARD:
        {
            return get_high_card_winner(hand1, hand2);
        };

        case ONE_PAIR:
        {
            int32 hand1_value = get_one_pair_value(hand1);
            int32 hand2_value = get_one_pair_value(hand2);

            if (hand1_value > hand2_value)
            {
                return WINNER_PLAYER_ONE;
            }
            else if (hand2_value > hand1_value)
            {
                return WINNER_PLAYER_TWO;
            }
            else
            {
                hand_t temp_hand1;
                for (int32 i = 0; i < NUM_CARDS; ++i)
                {
                    temp_hand1.cards[i].value = hand1.cards[i].value == hand1_value ? 0 : hand1.cards[i].value;
                    temp_hand1.cards[i].suit = hand1.cards[i].suit;
                    temp_hand1.sorted_cards[i] = hand1.sorted_cards[i];
                }

                hand_t temp_hand2;
                for (int32 i = 0; i < NUM_CARDS; ++i)
                {
                    temp_hand2.cards[i].value = hand2.cards[i].value == hand2_value ? 0 : hand2.cards[i].value;
                    temp_hand2.cards[i].suit = hand2.cards[i].suit;
                    temp_hand2.sorted_cards[i] = hand2.sorted_cards[i];
                }

                return get_high_card_winner(temp_hand1, temp_hand2);
            }
        };

        case TWO_PAIRS:         printf("\nNOT HANDLED"); break;
        case THREE_OF_A_KIND:   printf("\nNOT HANDLED"); break;
        case STRAIGHT:          printf("\nNOT HANDLED"); break;
        case FLUSH:             printf("\nNOT HANDLED"); break;
        case FULL_HOUSE:        printf("\nNOT HANDLED"); break;
        case FOUR_OF_A_KIND:    printf("\nNOT HANDLED"); break;
        case STRAIGHT_FLUSH:    printf("\nNOT HANDLED"); break;
        case ROYAL_FLUSH:       printf("\nNOT HANDLED"); break;
    }

    return WINNER_NONE;
}

int32 get_high_card_winner(hand_t hand1, hand_t hand2)
{
    for (int32 i = 1; i <= 5; ++i)
    {
        int32 hand1_highest = get_n_highest_card_value(hand1, i);
        int32 hand2_highest = get_n_highest_card_value(hand2, i);

        if (hand1_highest > hand2_highest)
        {
            return WINNER_PLAYER_ONE;
        }
        else if (hand2_highest > hand1_highest)
        {
            return WINNER_PLAYER_TWO;
        }
    }

    return WINNER_NONE;
}

int32 get_highest_hand_rank(hand_t hand)
{
    if (has_royal_flush(hand))
    { return ROYAL_FLUSH; }

    if (has_straight_flush(hand))
    { return STRAIGHT_FLUSH; }

    if (has_four_of_a_kind(hand))
    { return FOUR_OF_A_KIND; }

    if (has_full_house(hand))
    { return FULL_HOUSE; }

    if (has_flush(hand))
    { return FLUSH; }

    if (has_straight(hand))
    { return STRAIGHT; }

    if (has_three_of_a_kind(hand))
    { return THREE_OF_A_KIND; }

    if (has_two_pairs(hand))
    { return TWO_PAIRS; }

    if (has_one_pair(hand))
    { return ONE_PAIR; }

    return HIGH_CARD;
}

int32 get_n_highest_card_value(hand_t hand, int32 n)
{
    if (n < 1) { n = 1; }
    else if (n > 5) { n = 5; }

    if (n == 1) { return hand.sorted_cards[4]; }

    if (n == 2)
    {
        for (int32 i = 3; i >= 0; --i)
        {
            if (hand.sorted_cards[i] != hand.sorted_cards[4])
            { return hand.sorted_cards[i]; }
        }
    }

    int32 j = 1;
    int32 temp = hand.sorted_cards[4];

    for (int32 i = 3; i >= 0; --i)
    {
        if (hand.sorted_cards[i] != temp)
        {
            ++j;
            temp = hand.sorted_cards[i];
            if (j == n) { return temp; }
        }
    }

    return temp;
}

int32 get_one_pair_value(hand_t hand)
{
    for (int32 i = 0; i < NUM_CARDS - 1; ++i)
    {
        if (hand.sorted_cards[i] == hand.sorted_cards[i + 1])
        {
            if (i == 0)
            {
                if (hand.sorted_cards[0] != hand.sorted_cards[2])
                { return hand.sorted_cards[i]; }
            }
            else if (hand.sorted_cards[i] != hand.sorted_cards[i - 1])
            { return hand.sorted_cards[i]; }
        }
    }

    return 0;
}

int32 get_num_pairs(hand_t hand)
{
    int32 num_pairs = 0;
    for (int32 i = 0; i < NUM_CARDS - 1; ++i)
    {
        if (hand.sorted_cards[i] == hand.sorted_cards[i + 1])
        {
            if (i == 0)
            {
                if (hand.sorted_cards[0] != hand.sorted_cards[2])
                { ++num_pairs; }
            }
            else if (hand.sorted_cards[i] != hand.sorted_cards[i - 1])
            {
                ++num_pairs;
            }
        }
    }

    return num_pairs;
}

int32 read_poker_file()
{
    FILE *fp;
    char str[BUFFER_SIZE];

    fp = fopen("p054_poker.txt", "r");
    if (fp == 0)
    {
        printf("\n\nCould not open file %s", "p054_poker.txt");
        return 1;
    }

    int32 hand = 0;
    while (fgets(str, BUFFER_SIZE, fp) != NULL)
    {
        int32 card = 0;
        for (int32 i = 0; i < 29; i += 3)
        {
            int32 value = 0;
            switch (str[i])
            {
                case '2': value = TWO;      break;
                case '3': value = THREE;    break;
                case '4': value = FOUR;     break;
                case '5': value = FIVE;     break;
                case '6': value = SIX;      break;
                case '7': value = SEVEN;    break;
                case '8': value = EIGHT;    break;
                case '9': value = NINE;     break;
                case 'T': value = TEN;      break;
                case 'J': value = JACK;     break;
                case 'Q': value = QUEEN;    break;
                case 'K': value = KING;     break;
                case 'A': value = ACE;      break;
            }

            int32 suit = 0;
            switch (str[i + 1])
            {
                case 'C': suit = CLUBS;     break;
                case 'H': suit = HEARTS;    break;
                case 'S': suit = SPADES;    break;
                case 'D': suit = DIAMONDS;  break;
            }

            if (card < 5)
            {
                player_one[hand].cards[card].value = value;
                player_one[hand].cards[card].suit = suit;
            }
            else
            {
                player_two[hand].cards[card - 5].value = value;
                player_two[hand].cards[card - 5].suit = suit;
            }

            ++card;
        }

        for (int32 k = 0; k < 5; ++k)
        {
            player_one[hand].sorted_cards[k] = player_one[hand].cards[k].value;
            player_two[hand].sorted_cards[k] = player_two[hand].cards[k].value;
        }

        bubble_sort(player_one[hand].sorted_cards, NUM_CARDS);
        bubble_sort(player_two[hand].sorted_cards, NUM_CARDS);

        ++hand;
    }

    fclose(fp);
    return 0;
}

void bubble_sort(int32 array[], int32 size)
{
    for (int32 i = 0; i < size - 1; ++i)
    {
        for (int32 j = 0; j < size - i - 1; ++j)
        {
            if (array[j] > array[j + 1])
            {
                swap(&array[j], &array[j + 1]);
            }
        }
    }
}

void swap(int* a, int* b)
{
    int32 temp = *a;
    *a = *b;
    *b = temp;
}
