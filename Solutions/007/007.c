#include <stdio.h>

#ifndef bool
#define bool int
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif


bool is_prime(int number);

int main()
{
    printf("Project Euler 007 - 10001st prime\n"
            "https://projecteuler.net/problem=7");

    int num_primes = 0;
    int answer = 2;

    while (true)
    {
        if (is_prime(answer))
        {
            ++num_primes;
            if (num_primes == 10001)
            { break; }
        }

        ++answer;
    }

    printf("\n\nAnswer: %i", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

bool is_prime(int number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return true; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return false; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return false; }

        i += 6;
    }

    return true;
}