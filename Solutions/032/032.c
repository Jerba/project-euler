#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

typedef int32_t int32;

/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
*/

int32 is_pandigital(int32 number);
int32 number_contains_zero(int32 number);
int32 concat_int32(int32 a, int32 b);
int32 get_num_digits(int32 number);

int main()
{
    printf("Project Euler 032 - Pandigital products\n"
            "https://projecteuler.net/problem=32");

    int32 answer = 0;
    int32 num_total_digits = 0;

    int32 *products = (int32*)malloc(sizeof(int32) * 8);
    int32 products_size = 32;
    int32 products_used = 0;

    for (int32 i = 1; i <= 9876 - 1; ++i)
    {
        for (int32 j = i + 1; j <= 9876; ++j)
        {
            int32 p = i * j;
            num_total_digits = get_num_digits(i) + get_num_digits(j) + get_num_digits(p);

            if (num_total_digits > 9)
            { break; }

            if (num_total_digits == 9)
            {
                int32 concat = concat_int32(concat_int32(i, j), p);

                if (number_contains_zero(concat) || !is_pandigital(concat))
                { continue; }

                // printf("\n\n%d x %d = %d is pandigital.", i, j, p);

                int32 product_used = 0;
                for (int32 k = 0; k < products_used; ++k)
                {
                    if (products[k] == p)
                    {
                        product_used = 1;
                        break;
                    }
                }

                if (product_used)
                { continue; }

                answer += p;
                products[products_used] = p;
                ++products_used;

                if (products_used >= products_size)
                {
                    int32 new_size = products_size + 8;
                    int32 *temp_products = (int32*)realloc(products, sizeof(int32) * new_size);

                    if (temp_products != 0)
                    {
                        products = temp_products;
                        products_size = new_size;
                    }
                    else
                    {
                        answer = -1;
                        goto cleanup;
                    }
                }
            }
        }
    }

    cleanup:
    free(products);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 is_pandigital(int32 number)
{
    int32 num_digits = get_num_digits(number);
    int32 used_digits[10] = {0};

    for (int32 i = 0; i < num_digits; ++i)
    {
        int32 digit = number % 10;
        number /= 10;

        used_digits[digit] += 1;
        if (used_digits[digit] == 2)
        { return 0; }
    }

    return 1;
}

int32 number_contains_zero(int32 number)
{
    int32 num_digits = get_num_digits(number);

    for (int32 i = 0; i < num_digits; ++i)
    {
        int32 digit = number % 10;
        number /= 10;

        if (digit == 0)
        { return 1; }
    }

    return 0;
}

int32 concat_int32(int32 a, int32 b)
{
    int32 pow = 10;

    while (b >= pow)
    { pow *= 10; }

    return a * pow + b;        
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
