#include <stdio.h>
#include <time.h>
#include <string.h>

#define FOR_MAX 1000000
#define CHAIN_CACHE_LENGTH 1000000

/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Using the rule above and starting with 13, we generate the following sequence: 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*/

long long get_chain_length(long long number);
long long chain_cache[CHAIN_CACHE_LENGTH];

int main()
{
    printf("Project Euler 014 - Longest Collatz sequence\n"
            "https://projecteuler.net/problem=14");

    clock_t start = clock();
    memset(chain_cache, 0, sizeof(chain_cache));

    long long answer = 0;
    long long largest_chain = 0;

    for (long long i = 2; i < FOR_MAX; ++i)
    {
        //printf("\nGoing at %i", i);
        long long temp = get_chain_length(i);
        if (temp > largest_chain)
        {
            answer = i;
            largest_chain = temp;
        }
    }

    clock_t end = clock();
    double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("\n\nStarting number %lli produces chain of %lli numbers.", answer, largest_chain);
    printf("\n\nAnswer: %lli", answer);

    printf("\n\nProgram took %fmS to execute", (cpu_time_used * 1000.0));
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

long long get_chain_length(long long number)
{
    if (chain_cache[number] != 0)
    { return chain_cache[number]; }

    if (number == 0) { return 0; }
    if (number < 0) { number *= -1; }

    long long chain = 1;
    long long temp = number;
    while (temp != 1)
    {
        if ((temp % 2) == 0)
        { temp = (temp / 2); }
        else
        { temp = ((temp * 3) + 1); }

        if (temp < CHAIN_CACHE_LENGTH && chain_cache[temp] != 0)
        {
            chain += chain_cache[temp];
            break;
        }
        else
        { ++chain; }
    }

    chain_cache[number] = chain;
    return chain;
}