#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define NUM_NON_TRIVIAL_FRACTIONS 4

typedef int32_t int32;

/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
*/

int32 get_num_digits(int32 number);
int32 find_first_common_digit(int32 a, int32 b);
int32 remove_digits(int32 number, int32 digit);
int32 gcd(int32 a, int32 b);

int main()
{
    printf("Project Euler 033 - Digit cancelling fractions\n"
            "https://projecteuler.net/problem=33");

    int32 numerators[NUM_NON_TRIVIAL_FRACTIONS];
    int32 denominators[NUM_NON_TRIVIAL_FRACTIONS];
    int32 found_fractions = 0;

    for (int32 i = 10; i < 99; ++i)
    {
        for (int32 j = i + 1; j <= 99; ++j)
        {
            if (i % 10 == 0 && j % 10 == 0)
            { continue; }

            int32 common_digit = find_first_common_digit(i, j);
            if (common_digit != -1)
            {
                int32 temp_i = remove_digits(i, common_digit);
                int32 temp_j = remove_digits(j, common_digit);
                if (temp_i == 0 || temp_j == 0)
                { continue; }

                double fraction = (double)i / (double)j;
                double temp_fraction = (double)temp_i / (double)temp_j;
                if (fraction == temp_fraction)
                {
                    numerators[found_fractions] = i;
                    denominators[found_fractions] = j;
                    found_fractions++;

                    if (found_fractions == NUM_NON_TRIVIAL_FRACTIONS)
                    { goto done; }
                }
            }
        }
    }

    done:
    int32 numerator_product     = numerators[0];
    int32 denominator_product   = denominators[0];
    for (int32 i = 1; i < NUM_NON_TRIVIAL_FRACTIONS; ++i)
    {
        numerator_product   *= numerators[i];
        denominator_product *= denominators[i];
    }

    int32 answer = denominator_product / gcd(numerator_product, denominator_product);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)(floor(log10(abs(number)))) + 1;
}

int32 find_first_common_digit(int32 a, int32 b)
{
    if (a < 0) { a *= -1; }
    if (b < 0) { b *= -1; }

    int32 len_a = get_num_digits(a);
    int32 len_b = get_num_digits(b);

    if (len_a != len_b)
    { return -1; }

    int32 temp_a = a;
    int32 temp_b = b;

    int32 digits_a[10] = {0};
    int32 digits_b[10] = {0};

    for (int32 i = 0; i < len_a; ++i)
    {
        int32 digit_a = temp_a == 0 ? 0 : temp_a % 10;
        temp_a = temp_a == 0 ? 0 : temp_a / 10;
        digits_a[digit_a]++;

        int32 digit_b = temp_b == 0 ? 0 : temp_b % 10;
        temp_b = temp_b == 0 ? 0 : temp_b / 10;
        digits_b[digit_b]++;
    }

    for (int32 i = 0; i < 10; ++i)
    {
        if (digits_a[i] != 0 && digits_b[i] != 0)
        { return i; }
    }

    return -1;
}

int32 remove_digits(int32 number, int32 digit)
{
    if (number == 0)
    { return 0; }

    int32 temp_digit = number % 10;
    int32 temp = remove_digits(number / 10, digit);

    if (digit != temp_digit)
    { temp = temp * 10 + temp_digit; }

    return temp;
}

int32 gcd(int32 a, int32 b)
{
    if (a == 0)
    {
        return b;
    }

    return gcd(b % a, a);
}
