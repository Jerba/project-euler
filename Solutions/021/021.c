#include <stdio.h>
#include <stdint.h>

typedef int32_t int32;

/*
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*/

int32 d(int32 n);

int main()
{
    printf("Project Euler 021 - Amicable numbers\n"
            "https://projecteuler.net/problem=21");

    int32 answer = 0;

    for (int32 a = 2; a < 10000; ++a)
    {
        int32 b = d(a);
        if (a != b && d(b) == a)
        {
            answer += a;
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 d(int32 n)
{
    if (n <= 1) { return 0; }
    if (n == 2) { return 1; }

    int32 sum = 1;
    for (int32 i = 2; i < n; ++i)
    {
        if (n % i == 0)
        { sum += i; }
    }

    return sum;
}
