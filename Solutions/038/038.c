#include <stdio.h>
#include <stdint.h>
#include <math.h>

typedef int32_t int32;

/*
Take the number 192 and multiply it by each of 1, 2, and 3:

    192 × 1 = 192
    192 × 2 = 384
    192 × 3 = 576

By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?
*/

int32 is_pandigital(int32 number);
int32 concat_int32(int32 a, int32 b);
int32 get_num_digits(int32 number);

int main()
{
    printf("Project Euler 038 - Pandigital multiples\n"
            "https://projecteuler.net/problem=38");

    int32 answer = 0;
    int32 concatenated = 0;
    int32 i = 0, j = 0;

    for (i = 9; i <= 9876; ++i)
    {
        concatenated = i;
        for (j = 2; ; ++j)
        {
            int32 temp = i * j;
            concatenated = concat_int32(concatenated, temp);

            if (!is_pandigital(concatenated))
            { break; }

            int32 num_digits = get_num_digits(concatenated);
            if (num_digits == 9)
            {
                if (concatenated > answer)
                { answer = concatenated; }
            }
            else if (num_digits > 9)
            { break; }
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 is_pandigital(int32 number)
{
    int32 num_digits = get_num_digits(number);
    int32 used_digits[10] = {0};

    for (int32 i = 0; i < num_digits; ++i)
    {
        int32 digit = number % 10;
        number /= 10;

        used_digits[digit] += 1;
        if (used_digits[digit] == 2)
        { return 0; }
    }

    return 1;
}

int32 concat_int32(int32 a, int32 b)
{
    int32 pow = 10;

    while (b >= pow)
    { pow *= 10; }

    return a * pow + b;        
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
