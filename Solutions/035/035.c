#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define PRIME_CACHE_SIZE 1000000
#define NUM_PRIMES 78498

typedef int32_t int32;

/*
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 get_num_digits(int32 number);

int32 primes[NUM_PRIMES];
int32 prime_cache[PRIME_CACHE_SIZE];

int main()
{
    printf("Project Euler 035 - Circular primes\n"
            "https://projecteuler.net/problem=");

    fill_prime_cache();
    int32 answer = 0;

    for (int32 i = 0; i < NUM_PRIMES; ++i)
    {
        int32 prime         = primes[i];
        int32 num_digits    = get_num_digits(prime);
        int32 pow_ten       = (int32)pow(10, num_digits - 1);
        int32 temp          = prime;
        int32 is_circular   = 1;

        for (int32 j = 0; j < num_digits - 1; ++j)
        {
            int32 digit = temp / pow_ten;

            int32 rotated = ((temp * 10) + digit) - (digit * pow_ten * 10);
            if (!prime_cache[rotated])
            {
                is_circular = 0;
                break;
            }

            temp = rotated;
        }

        if (is_circular)
        { ++answer; }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return 1; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return 0; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
