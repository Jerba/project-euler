#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

typedef int32_t int32;

/*
An irrational decimal fraction is created by concatenating the positive integers:

0.123456789101112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
*/

int32 get_num_digits(int32 number);

int main()
{
    printf("Project Euler 040 - Champernowne's constant\n"
            "https://projecteuler.net/problem=40");

    int32 *fraction = (int32*)malloc(sizeof(int32) * 1048576);

    int32 i = 0, j = 0, k = 0;
    int32 num_digits = 0;

    for (i = 0; k <= 1000000; ++i)
    {
        int32 number = i;
        num_digits = get_num_digits(number);

        for (j = 0; j < num_digits; ++j)
        {
            fraction[k + (num_digits - j - 1)] = number % 10;
            number /= 10;
        }

        k += num_digits;
    }

    int32 answer = fraction[1];
    for (i = 10; i <= 1000000; i *= 10)
    { answer *= fraction[i]; }

    free(fraction);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
