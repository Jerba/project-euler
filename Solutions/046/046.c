#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define PRIME_CACHE_SIZE 1000000
#define NUM_PRIMES 78498

typedef int32_t int32;

/*
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9   = 7 + 2×1^2
15  = 7 + 2×2^2
21  = 3 + 2×3^2
25  = 7 + 2×3^2
27  = 19 + 2×2^2
33  = 31 + 2×1^2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 is_composite(int32 number);
int32 can_be_written_as_sum_of_prime_and_twice_a_square(int32 number);

int32 primes[NUM_PRIMES];
int32 prime_cache[PRIME_CACHE_SIZE];
int32 prime_cache_filled = 0;

int main()
{
    printf("Project Euler 046 - Goldbach's other conjecture\n"
            "https://projecteuler.net/problem=46");

    fill_prime_cache();

    int32 answer = 0;
    for (int32 i = 35; ; i += 2)
    {
        if (!is_composite(i)) { continue; }

        if (!can_be_written_as_sum_of_prime_and_twice_a_square(i))
        {
            answer = i;
            break;
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }

    prime_cache_filled = 1;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (prime_cache_filled && number < PRIME_CACHE_SIZE) { return prime_cache[number]; }
    if (number == 2 || number == 3) { return 1; }
    if (number % 2 == 0 || number % 3 == 0 || number <= 1) { return 0; }

    int32 i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 is_composite(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number < 4) { return 0; }
    if (prime_cache_filled && number < PRIME_CACHE_SIZE) { return !prime_cache[number]; }

    return !is_prime(number);
}

int32 can_be_written_as_sum_of_prime_and_twice_a_square(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 0) { return 0; }

    for (int32 i = 0; i < NUM_PRIMES; ++i)
    {
        int32 temp = number - primes[i];
        if (temp <= 0) { return 0; }

        int32 squareroot = (int32)sqrt(temp / 2);
        if (primes[i] + (2 * squareroot * squareroot) == number)
        { return 1; }
    }

    return 0;
}
