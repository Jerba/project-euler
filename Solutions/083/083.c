#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define SOLID 0

typedef int32_t int32;

typedef struct node_t
{
    int32 col, row;
    int32 g, h, f;
    int32 w;
    int32 parent_index;
} node_t;

typedef struct node_array_t
{
    node_t *nodes;
    size_t used;
    size_t size;
} node_array_t;

int32 read_grid_file(const char *path);

int32 node_array_init(node_array_t *n_arr, size_t size);
int32 node_array_free(node_array_t *n_arr);
int32 node_array_clear(node_array_t *n_arr);
int32 node_array_sort(node_array_t *n_arr);

int32 node_array_push(node_array_t *n_arr, int32 col, int32 row, int32 w);
int32 node_array_push_copy(node_array_t *n_arr, node_t node);
node_t node_array_pop(node_array_t *n_arr);
node_t node_array_pop_front(node_array_t *n_arr);

int32 are_nodes_equal(node_t a, node_t b);
int32 node_array_contains(node_array_t *n_arr, node_t node);
int32 node_array_get_index(node_array_t *n_arr, node_t node);

void node_print(node_t node);
void node_array_print(node_array_t *n_arr);

int32 get_grid_index(int32 x, int32 y);
int32 get_h(node_t a, node_t b);
int32 find_path(node_array_t *grid_arr, int32 x1, int32 y1, int32 x2, int32 y2, int32 *out_cost);

int32 *grid         = 0;
int32 grid_width    = 5;
int32 grid_size     = 5 * 5;
char *map;

int32 allow_diagonals   = 0;
int32 allow_up          = 1;
int32 allow_down        = 1;
int32 allow_left        = 1;
int32 allow_right       = 1;
int32 show_map          = 1;

node_array_t grid_nodes;
node_array_t closed_set;
node_array_t open_set;

static const node_t node_empty = { -1, -1, -1, -1, -1, -1, -1 };


/*
NOTE: This problem is a significantly more challenging version of Problem 81.

In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by moving left, right, up, and down, is indicated in bold red and is equal to 2297.

|  +131   673  +234  +103   +18   |
|  +201   +96  +342   965  +150   |
|   630   803   746  +422  +111   | 
|   537   699   497  +121   956   |
|   805   732   524   +37  +331   |

Find the minimal path sum from the top left to the bottom right by moving left, right, up, and down in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing an 80 by 80 matrix.
*/

int main()
{
    printf("Project Euler 083 - Path sum: four ways\n"
            "https://projecteuler.net/problem=83");

    grid_width = 80;
    grid_size = grid_width * grid_width;

    grid = (int32*)malloc(sizeof(int32) * grid_size);
    map = (char*)malloc(sizeof(int32) * grid_size);

    if (read_grid_file("p083_matrix.txt") != 0)
    {
        free(grid);
        free(map);
        return 0;
    }

    node_array_init(&grid_nodes, grid_size);
    node_array_init(&closed_set, 8);
    node_array_init(&open_set, 8);

    for (int32 i = 0; i < grid_size; ++i)
    {
        int32 col = i % grid_width;
        int32 row = i / grid_width;

        node_array_push(&grid_nodes, col, row, grid[i]);
    }

    printf("\n\nFinding path from %d, %d to %d, %d", 0, 0, grid_width - 1, grid_width - 1);

    int32 answer = 0;
    int32 result = find_path(&grid_nodes, 0, 0, grid_width - 1, grid_width - 1, &answer);

    printf("\n\nPathfinding result: %d, cost: %d", result, answer);

    free(grid);
    free(map);

    node_array_free(&grid_nodes);
    node_array_free(&closed_set);
    node_array_free(&open_set);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 read_grid_file(const char *path)
{
    FILE *fp;

    fp = fopen(path, "r");
    if (fp == 0)
    {
        printf("\nCould not open file %s", path);
        return 1;
    }

    fseek(fp, 0L, SEEK_END);
    int32 size = ftell(fp);
    rewind(fp);

    if (size <= 0)
    {
        fclose(fp);
        return 2;
    }

    char *str = (char*)malloc(sizeof(char) * size);
    fread(str, sizeof(char), size, fp);

    char *value_str;
    int32 i = 0;

    for (value_str = strtok(str, ",\n"); value_str != 0; value_str = strtok(0, ",\n"))
    {
        grid[i] = atoi(value_str);
        ++i;
    }

    free(str);
    fclose(fp);
    return 0;
}

int32 node_array_init(node_array_t *n_arr, size_t size)
{
    if (n_arr == 0) { return 1; }
    if (size <= 0) { return 2; }

    if (n_arr->nodes != 0)
    {
        if (size >= n_arr->size)
        {
            node_t *temp = (node_t*)realloc(n_arr->nodes, size * sizeof(node_t));
            if (temp == 0)
            {
                return 3;
            }
            else
            {
                n_arr->size = size;
                n_arr->nodes = temp;
                return 0;
            }
        }

        return 0;
    }

    n_arr->nodes    = (node_t*)malloc(size * sizeof(node_t));
    n_arr->used     = 0;
    n_arr->size     = size;

    for (int32 i = 0; i < n_arr->size; ++i)
    { n_arr->nodes[i] = node_empty; }

    return 0;
}

int32 node_array_free(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return 1; }

    free(n_arr->nodes);
    n_arr->used     = 0;
    n_arr->size     = 0;
    n_arr->nodes    = 0;

    return 0;
}

int32 node_array_clear(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return 1; }

    for (int32 i = 0; i < n_arr->size; ++i)
    {
        n_arr->nodes[i] = node_empty;
    }

    n_arr->used = 0;

    return 0;
}

int32 node_array_sort(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return 1; }

    size_t size = n_arr->used;
    for (int32 i = 0; i < size - 1; ++i)
    {
        for (int32 j = 0; j < size - i - 1; ++j)
        {
            if (n_arr->nodes[j].f > n_arr->nodes[j + 1].f)
            {
                node_t temp         = n_arr->nodes[j];
                n_arr->nodes[j]     = n_arr->nodes[j + 1];
                n_arr->nodes[j + 1] = temp;
            }
        }
    }

    return 0;
}

int32 node_array_push(node_array_t *n_arr, int32 col, int32 row, int32 w)
{
    node_t node;
    node.col = col;
    node.row = row;
    node.g = 0;
    node.h = 0;
    node.f = 0;
    node.w = w;
    node.parent_index = -1;

    return node_array_push_copy(n_arr, node);
}

int32 node_array_push_copy(node_array_t *n_arr, node_t node)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return 1; }

    if (n_arr->used == n_arr->size)
    {
        size_t new_size = n_arr->size + 8;

        node_t *temp = (node_t*)realloc(n_arr->nodes, new_size * sizeof(node_t));
        if (temp == 0)
        {
            return 2;
        }
        else
        {
            n_arr->size = new_size;
            n_arr->nodes = temp;
        }
    }

    size_t used = n_arr->used;
    n_arr->nodes[used] = node;
    n_arr->used++;

    return 0;
}

node_t node_array_pop(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return node_empty; }
    if (n_arr->used <= 0) { return node_empty; }

    node_t node = n_arr->nodes[n_arr->used - 1];
    n_arr->nodes[n_arr->used - 1] = node_empty;
    n_arr->used--;

    return node;
}

node_t node_array_pop_front(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { return node_empty; }
    if (n_arr->used <= 0) { return node_empty; }

    node_t node = n_arr->nodes[0];
    for (int32 i = 1; i < n_arr->used; ++i)
    {
        n_arr->nodes[i - 1] = n_arr->nodes[i];
    }

    n_arr->used--;
    return node;
}

int32 are_nodes_equal(node_t a, node_t b)
{
    return a.col == b.col && a.row == b.row && a.w == b.w;
}

int32 node_array_contains(node_array_t *n_arr, node_t node)
{
    if (n_arr == 0 || n_arr->nodes == 0 || n_arr->used == 0) { return 0; }

    for (int32 i = 0; i < n_arr->used; ++i)
    {
        if (are_nodes_equal(node, n_arr->nodes[i]))
        { return 1; }
    }

    return 0;
}

int32 node_array_get_index(node_array_t *n_arr, node_t node)
{
    if (n_arr == 0 || n_arr->nodes == 0 || n_arr->used == 0) { return -1; }

    for (int32 i = 0; i < n_arr->used; ++i)
    {
        if (are_nodes_equal(node, n_arr->nodes[i]))
        { return i; }
    }

    return -1;
}

void node_array_print(node_array_t *n_arr)
{
    if (n_arr == 0 || n_arr->nodes == 0) { printf("\nNode Array: NULL"); return; }

    printf("\nNode Array\n  Size: %d\n  Used: %d\n  Nodes:\n", n_arr->size, n_arr->used);

    for (int32 i = 0; i < n_arr->used; ++i)
    {
        node_t node = n_arr->nodes[i];
        printf("    col %d, row %d, w %d, f %d, \n",
                    node.col, node.row, node.w, node.f);
    }
}

void node_print(node_t node)
{
    printf("\nNode:\n    col %d, row %d, w %d, \n",
                        node.col, node.row, node.w);
}

int32 get_grid_index(int32 x, int32 y)
{
    return y * grid_width + x;
}

int32 get_h(node_t a, node_t b)
{
    int32 x = abs(a.col - b.col);
    int32 y = abs(a.row - b.row);

    if (x > y) { return 14 * y + 10 * (x - y); }
    else { return 14 * x + 10 * (y - x); }
}

int32 find_path(node_array_t *grid_arr, int32 x1, int32 y1, int32 x2, int32 y2, int32 *out_cost)
{
    *out_cost = 0;
    if (grid_arr == 0) { return 1; }
    if (grid_arr->nodes == 0) { return 2; }

    int32 start_index = get_grid_index(x1, y1);
    int32 end_index   = get_grid_index(x2, y2);

    if (start_index >= grid_arr->used || start_index < 0 ||
        end_index >= grid_arr->used || end_index < 0)
    { return 3; }
    
    node_array_clear(&open_set);
    node_array_clear(&closed_set);

    node_t start_node = grid_arr->nodes[start_index];
    node_t end_node = grid_arr->nodes[end_index];
    node_array_push_copy(&open_set, start_node);

    while (open_set.used > 0)
    {
        node_array_sort(&open_set);
        node_t current = node_array_pop_front(&open_set);
        int32 current_index = get_grid_index(current.col, current.row);

        if (current_index == end_index)
        {
            memset(map, '.', sizeof(char) * (grid_size));

            //printf("\n\nPath (reverse): ");

            int32 path_index = current_index;

            node_t path_node = grid_arr->nodes[path_index];
            grid_arr->nodes[path_index].parent_index = -1;
            map[path_index] = ',';
            int32 cost = path_node.w;

            for (int32 i = 0; i < grid_arr->used; ++i)
            {
                if (path_index == start_index || path_node.parent_index == -1)
                { break; }

                path_index = path_node.parent_index;
                path_node = grid_arr->nodes[path_index];
                cost += path_node.w;

                grid_arr->nodes[path_index].parent_index = -1;
                map[path_index] = ',';
            }

            if (show_map)
            {
                printf("\n\nMap:");
                for (int32 i = 0; i < (grid_width * grid_width); ++i)
                {
                    if (i % (grid_width) == 0) { printf("\n"); }
                    printf("%c", map[i]);
                }
            }

            *out_cost = cost;
            return 0;
        }

        node_array_push_copy(&closed_set, current);

        node_t neighbours[8];
        int32 num = 0;
        for (int32 i = -1; i <= 1; ++i)
        {
            for (int32 j = -1; j <= 1; ++j)
            {
                if (i == 0 && j == 0)
                { continue; }

                if (!allow_diagonals && i != 0 && j != 0)
                { continue; }

                if (!allow_up && j == -1)
                { continue; }

                if (!allow_down && j == 1)
                { continue; }

                if (!allow_left && i == -1)
                { continue; }

                if (!allow_right && i == 1)
                { continue; }

                int32 col = i + current.col;
                int32 row = j + current.row;
                if (col < 0 || col >= grid_width) { continue; }
                if (row < 0 || row >= grid_width) { continue; }

                int32 index = get_grid_index(col, row);
                neighbours[num] = grid_arr->nodes[index];
                ++num;
            }
        }

        for (int32 i = 0; i < num; ++i)
        {
            node_t neighbour = neighbours[i];

            if (neighbour.w == SOLID || node_array_contains(&closed_set, neighbour))
            { continue; }

            int32 cost = current.g + get_h(current, neighbour) * neighbour.w;
            if (cost < neighbour.g || !node_array_contains(&open_set, neighbour))
            {
                int32 index = get_grid_index(neighbour.col, neighbour.row);

                neighbour.g = cost;
                neighbour.h = get_h(neighbour, end_node);
                neighbour.f = neighbour.g + neighbour.h;

                grid_arr->nodes[index].parent_index = current_index;

                int32 open_index = node_array_get_index(&open_set, neighbour);
                if (open_index == -1)
                { node_array_push_copy(&open_set, neighbour); }
                else
                { open_set.nodes[open_index] = neighbour; }
            }
        }
    }

    return 4;
}
