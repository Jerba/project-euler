#include <stdio.h>
#include <stdint.h>

#define NUM_KEYS 50
#define KEY_LEN 3
#define BUFFER_SIZE 8

typedef int32_t int32;

typedef struct digit_t
{
    int32 digit;
    int32 num_appearances;
    double average_position;
} digit_t;

/*
A common security method used for online banking is to ask the user for three random characters from a passcode.
For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
*/

void sort_by_position(digit_t array[], int32 size);
int32 read_keylog_file();

int32 keys[NUM_KEYS * KEY_LEN] = { 0 };

int main()
{
    printf("Project Euler 079 - Passcode derivation\n"
            "https://projecteuler.net/problem=79");

    if (read_keylog_file())
        return 0;

    digit_t digits[10] = { 0 };

    for (int32 i = 0; i < 10; ++i)
    {
        digits[i].digit = i;
        digits[i].num_appearances = 0;
        digits[i].average_position = 0;
    }

    for (int32 i = 0; i < NUM_KEYS * KEY_LEN; ++i)
    {
        digits[keys[i]].num_appearances++;
        digits[keys[i]].average_position += (double)((i % KEY_LEN) + 1);
    }

    for (int32 i = 0; i < 10; ++i)
    {
        if (digits[i].num_appearances != 0)
            digits[i].average_position /= digits[i].num_appearances;
    }

    sort_by_position(digits, 10);

    printf("\n\nAnswer: ");
    for (int32 i = 0; i < 10; ++i)
    {
        if (digits[i].num_appearances > 0)
            printf("%i", digits[i].digit);
    }

    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
    
}

void sort_by_position(digit_t array[], int32 size)
{
    for (int32 i = 0; i < size - 1; ++i)
    {
        for (int32 j = 0; j < size - i - 1; ++j)
        {
            if (array[j].average_position > array[j + 1].average_position)
            {
                digit_t temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}

int32 read_keylog_file()
{
    FILE *fp;
    char str[BUFFER_SIZE];

    fp = fopen("p079_keylog.txt", "r");
    if (fp == 0)
    {
        printf("\n\nCould not open p079_keylog.txt");
        return 1;
    }

    int32 index = 0;
    while (fgets(str, BUFFER_SIZE, fp) != NULL)
    {
        for (int32 i = 0; i < KEY_LEN; ++i)
            keys[index++] = (int32)(str[i] - '0');
    }

    fclose(fp);
    return 0;
}
