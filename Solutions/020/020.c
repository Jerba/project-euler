#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define MAX_DIGITS 65536
typedef int32_t int32;
typedef uint64_t uint64;

/*
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
*/

uint64 get_factorial_digit_sum(int32 number);
int32 multiply(int32 value[MAX_DIGITS], int32 factor);
int32 find_start_index(int32 number[MAX_DIGITS]);
int32 get_num_digits(int32 number);

int32 main()
{
    printf("Project Euler 020 - Factorial digit sum\n"
            "https://projecteuler.net/problem=20");

    uint64 answer = get_factorial_digit_sum(100);

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

uint64 get_factorial_digit_sum(int32 number)
{
    if (number <= 0)
    { return 0; }

    if (number == 1)
    { return 1; }

    printf("\n\nCalculating factorial %d!", number);

    int32 number_array[MAX_DIGITS] = {0};
    int32 num_digits = get_num_digits(number);
    int32 temp = number;

    for (int32 i = 0; i < num_digits; ++i)
    {
        int32 digit = temp % 10;
        temp /= 10;

        number_array[MAX_DIGITS - 1 - i] = digit;
    }

    for (int32 factor = number - 1; factor > 1; --factor)
    {
        if (multiply(number_array, factor) != 0)
        { return 0; }
    }

    uint64 digit_sum = 0;
    int32 start_index = find_start_index(number_array);

    for (int32 i = start_index; i < MAX_DIGITS; ++i)
    {
        digit_sum += number_array[i];
    }

    printf("\n\nProduct: ");
    for (int32 i = start_index; i < MAX_DIGITS; ++i)
    {
        printf("%d", number_array[i]);
    }

    return digit_sum;
}

int32 multiply(int32 number[MAX_DIGITS], int32 factor)
{
    if (factor == 1)
    {
        return 0;
    }

    if (factor == 0)
    {
        memset(number, 0, sizeof(int32) * MAX_DIGITS);
        return 0;
    }

    if (factor < 0)
    {
        factor *= -1;
    }

    int32 original_number[MAX_DIGITS] = {0};
    memcpy(original_number, number, sizeof(original_number));
    memset(number, 0, sizeof(int32) * MAX_DIGITS);

    int32 i = 0, j = 0, carry = 0;

    int32 start_index   = find_start_index(original_number);
    int32 end_index     = MAX_DIGITS - 1;
    int32 num_digits    = get_num_digits(factor);
    int32 temp          = factor;

    // printf("\n\nMultiplying ");
    // for (i = start_index; i <= end_index; ++i)
    // {
        // printf("%d", original_number[i]);
    // }
    // printf(" with %d", factor);

    int32 temp_value[MAX_DIGITS] = {0};
    for (i = 0; i < num_digits; ++i)
    {
        memcpy(temp_value, original_number, sizeof(temp_value));

        int32 digit = temp % 10;
        temp /= 10;

        for (j = end_index; j >= start_index; --j)
        {
            int32 val = (original_number[j] * digit) + carry;

            carry = val / 10;
            val -= 10 * carry;
            temp_value[j] = val;
        }

        int32 index = start_index;

        if (carry > 0)
        {
            if (start_index - 1 < 0) { goto error; }
            --index;

            temp_value[index] = carry;
            carry = 0;
        }

        for (j = end_index; j >= index; --j)
        {
            if (j - i - 1 < 0) { goto error; }

            int32 temp_val = number[j - i] + temp_value[j];
            int32 temp_carry = temp_val / 10;
            temp_val -= 10 * temp_carry;

            number[j - i] = temp_val;
            number[j - i - 1] += temp_carry;
        }
    }

    start_index = find_start_index(number);

    // printf("\nProduct: ");
    // for (i = start_index; i <= end_index; ++i)
    // {
        // printf("%d", number[i]);
    // }

    return 0;

    error:
    memcpy(number, original_number, sizeof(number));
    printf("\n\nError: Number was too big to calculate.");

    return 1;
}

int32 find_start_index(int32 number[MAX_DIGITS])
{
    for (int32 i = 0; i < MAX_DIGITS; ++i)
    {
        if (number[i] != 0)
        {
            return i;
        }
    }

    return MAX_DIGITS - 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
