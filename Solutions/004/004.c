#include <stdio.h>

#ifndef bool
#define bool int
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
*/

bool is_palindrome(int value);

int main()
{
    printf("Project Euler 004 - Largest palindrome product\n"
            "https://projecteuler.net/problem=4");

    int answer = 0;
    for (int i = 100; i < 1000 - 1; ++i)
    {
        for (int j = i + 1; j < 1000; ++j)
        {
            int temp = i * j;

            if (is_palindrome(temp))
            {
                if (temp >= answer)
                {
                    answer = temp;
                }
            }
        }
    }

    printf("\n\nAnswer: %i", answer);

    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

bool is_palindrome(int value)
{
    char s[32];
    sprintf(s,"%i\0", value);

    if (value < 0) { value *= -1; }
    if (value < 10) { return true; }

    int j = 0;

    for (int i = 1; i < 32; ++i)
    {
        if (s[i] == '\0')
        {
            j = i - 1;
            break;
        }
    }

    for (int i = 0; i < j; ++i)
    {
        if (s[i] != s[j - i]) { return false; }
        if (i == j - 1) { break; }
    }

    return true;
}