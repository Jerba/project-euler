#include <stdio.h>
#include <stdint.h>

#define NUM_DIGITS 10
#define NUM_PERMUTATIONS 999999

typedef int32_t int32;

/*
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/

int32 permute(int32 digits[], int32 length);
int32 permute_n_times(int32 digits[], int32 length, int32 n);
int32 get_k(int32 arr[], int32 len);
int32 get_l(int32 arr[], int32 len, int32 k);
void reverse_array(int32 arr[], int32 len, int32 start);
void swap(int32 *a, int32 *b);

int main()
{
    printf("Project Euler 024 - Lexicographic permutations\n"
            "https://projecteuler.net/problem=24");

    int32 digits[NUM_DIGITS] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    int32 result = permute_n_times(digits, NUM_DIGITS, NUM_PERMUTATIONS);

    if (result != 0)
    {
        printf("\n\nFailed to get answer, error: %d", result);
    }
    else
    {
        printf("\n\nAnswer: ");
        for (int32 i = 0; i < NUM_DIGITS; ++i)
        {
            printf("%d", digits[i]);
        }
    }

    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 permute_n_times(int32 digits[], int32 length, int32 n)
{
    if (length <= 0 || n < 0)
    { return 1; }

    if (n == 0)
    { return 0; }

    for (int32 i = 0; i < n; ++i)
    {
        if (permute(digits, length) != 0)
        { return 2; }
    }

    return 0;
}

int32 permute(int32 digits[], int32 length)
{
    if (length <= 0) { return 1; }

    int32 k = get_k(digits, length);
    if (k == -1)
    { return 2; }

    int32 l = get_l(digits, length, k);
    if (l == -1)
    { return 3; }

    swap(&digits[k], &digits[l]);
    reverse_array(digits, length, k + 1);

    return 0;
}

int32 get_k(int32 arr[], int32 len)
{
    int32 k = -1;
    if (len <= 0) { return k; }

    for (int32 i = 0; i < len; ++i)
    {
        if (i + 1 < len)
        {
            if (arr[i] < arr[i + 1])
            {
                k = i;
            }
        }
    }

    return k;
}

int32 get_l(int32 arr[], int32 len, int32 k)
{
    int l = -1;
    if (len <= 0 || k >= len) { return l; }

    for (int i = k + 1; i < len; ++i)
    {
        if (arr[k] < arr[i])
        {
            l = i;
        }
    }

    return l;
}

void swap(int32* a, int32* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void reverse_array(int32 arr[], int32 len, int32 start)
{
    if (len <= 0 || start >= len || start < 0) { return; }

    int32 i, j;
    for (i = start, j = 0; j < ((len - start) / 2); ++i, ++j)
    {
        swap(&arr[i], &arr[len - j - 1]);
    }
}