#include <stdio.h>
#include <stdint.h>

typedef int32_t int32;

/*
The decimal number, 585 = 1001001001 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)
*/

int32 is_palindrome(int32 number);
int32 is_palindrome_binary(int32 number);

int main()
{
    printf("Project Euler 036 - Double-base palindromes\n"
            "https://projecteuler.net/problem=36");

    int32 answer = 0;
    for (int32 i = 1; i < 1000000; ++i)
    {
        if (is_palindrome(i) && is_palindrome_binary(i))
        { answer += i; }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 is_palindrome(int32 number)
{
    int32 temp = number;
    int32 remainder = 0;
    int32 reversed = 0;

    while (temp != 0)
    {
        remainder = temp % 10;
        reversed = reversed * 10 + remainder;
        temp /= 10;
    }

    return number == reversed;
}

int32 is_palindrome_binary(int32 number)
{
    int32 binary[32];
    int32 start = 0;

    for (int32 i = 0, n = number; n > 0; ++i)
    {
        binary[i] = n % 2;
        n /= 2;
        start = i;
    }

    for (int32 i = 0; i <= start / 2 && start != 0; ++i)
    {
        if (binary[i] != binary[start - i])
        { return 0; }
    }

    return 1;
}
