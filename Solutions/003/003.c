#include <stdio.h>

/*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*/

long long find_largest_prime_factor(long long number);

int main()
{
    printf("Project Euler 003 - Largest prime factor\n"
            "https://projecteuler.net/problem=3");

    long long answer = find_largest_prime_factor(600851475143);

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

long long find_largest_prime_factor(long long number)
{
    if (number <= 1) { return 0; }

    long long i = 2;
    long long largest = 0;

    while (number != 0)
    {
        if (number % i != 0)
        {
            ++i;
        }
        else
        {
            largest = number;
            number = number / i;

            if (number <= 1) { break; }
        }
    }

    return largest;
}










