#include <stdio.h>

#define GRID_SIZE 20
#define NUM_ADJACENTS 4

/*
What is the greatest product of four adjacent numbers in the same direction
(up, down, left, right, or diagonally) in the 20×20 grid?
*/

int grid[] =
{
     8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8,
    49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0,
    81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65,
    52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91,
    22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80,
    24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50,
    32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70,
    67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21,
    24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72,
    21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95,
    78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92,
    16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57,
    86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58,
    19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40,
    04, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66,
    88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69,
    04, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36,
    20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16,
    20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54,
     1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48,
};

int clamp(int value, int min, int max);
int get_grid_value(int x, int y);
int get_horizontally();
int get_vertically();
int get_diagonally();

int main()
{
    printf("Project Euler 011 - Largest product in a grid\n"
            "https://projecteuler.net/problem=11");

    int answer = 0;
    int biggest_horizontally = get_horizontally();
    int biggest_vertically = get_vertically();
    int biggest_diagonally = get_diagonally();

    if (biggest_horizontally > answer)
    { answer = biggest_horizontally; }

    if (biggest_vertically > answer)
    { answer = biggest_vertically; }

    if (biggest_diagonally > answer)
    { answer = biggest_diagonally; }

    printf("\n\nBiggest horizontally: %i\n"
        "Biggest vertically: %i\n"
        "Biggest diagonally: %i",
        biggest_horizontally,
        biggest_vertically,
        biggest_diagonally);

    printf("\n\nAnswer: %i", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int get_horizontally()
{
    int biggest = 0;

    for (int y = 0; y < GRID_SIZE; ++y)
    {
        for (int x = 0; x <= GRID_SIZE - NUM_ADJACENTS; ++x)
        {
            int temp = get_grid_value(x, y);
            for (int i = x + 1; i < x + NUM_ADJACENTS; ++i)
            { temp *= get_grid_value(i, y); }

            if (temp > biggest)
            { biggest = temp; }
        }
    }

    return biggest;
}

int get_vertically()
{
    int biggest = 0;

    for (int y = 0; y <= GRID_SIZE - NUM_ADJACENTS; ++y)
    {
        for (int x = 0; x < GRID_SIZE; ++x)
        {
            int temp = get_grid_value(x, y);
            for (int i = y + 1; i < y + NUM_ADJACENTS; ++i)
            { temp *= get_grid_value(x, i); }

            if (temp > biggest)
            { biggest = temp; }
        }
    }

    return biggest;
}

int get_diagonally()
{
    int biggest = 0;

    for (int y = 0; y <= GRID_SIZE - NUM_ADJACENTS; ++y)
    {
        
        for (int x = 0; x <= GRID_SIZE - NUM_ADJACENTS; ++x)
        {
            int temp = get_grid_value(x, y);
            for (int i = x + 1, j = y + 1; i < x + NUM_ADJACENTS; ++i, ++j)
            { temp *= get_grid_value(i, j); }

            if (temp > biggest)
            { biggest = temp; }
        }
    }

    for (int y = 0; y <= GRID_SIZE - NUM_ADJACENTS; ++y)
    {
        for (int x = GRID_SIZE - 1; x >= NUM_ADJACENTS - 1; --x)
        {
            int temp = get_grid_value(x, y);
            for (int i = x - 1, j = y + 1; i > x - NUM_ADJACENTS; --i, ++j)
            { temp *= get_grid_value(i, j); }

            if (temp > biggest)
            { biggest = temp; }
        }
    }

    return biggest;
}

int get_grid_value(int x, int y)
{
    x = clamp(x, 0, GRID_SIZE - 1);
    y = clamp(y, 0, GRID_SIZE - 1);
    return grid[y * GRID_SIZE + x];
}

int clamp(int value, int min, int max)
{
    if (value < min) { value = min; }
    else if (value > max) { value = max; }

    return value;
}
