#include <stdio.h>
#include <stdint.h>
#include <math.h>

typedef int32_t int32;

/*
If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?
*/

int main()
{
    printf("Project Euler 039 - Integer right triangles\n"
            "https://projecteuler.net/problem=39");

    int32 answer = 0;
    int32 max_solutions = 0;
    for (int32 p = 3; p <= 1000; ++p)
    {
        int32 solutions = 0;

        for (int32 a = 1; a < p / 3; ++a)
        {
            for (int32 b = a + 1; b < p / 2; ++b)
            {
                int32 c = p - a - b;
                if ((a * a) + (b * b) != (c * c))
                { continue; }

                if (a + b + c == p)
                {
                    ++solutions;
                    if (solutions > max_solutions)
                    {
                        max_solutions = solutions;
                        answer = p;
                    }
                }
            }
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}
