#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define PRIME_CACHE_SIZE 1000000
#define NUM_PRIMES 78498

typedef int32_t int32;

/*
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?

*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 get_number_of_distinct_prime_factors(int32 number);

int32 prime_cache[PRIME_CACHE_SIZE] = { 0 };
int32 primes[NUM_PRIMES] = { 0 };
int32 prime_cache_filled = 0;

int main()
{
    printf("Project Euler 047 - Distinct primes factors\n"
            "https://projecteuler.net/problem=47");

    fill_prime_cache();

    int32 i = 0, streak = 0;
    int32 numbers[4] = { 0 };

    for (i = 210; streak < 4; ++i)
    {
        if (get_number_of_distinct_prime_factors(i) == 4)
        {
            numbers[streak] = i;
            ++streak;
        }
        else
        { streak = 0; }
    }

    int32 answer = numbers[0];

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }

    prime_cache_filled = 1;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (prime_cache_filled && number < PRIME_CACHE_SIZE) { return prime_cache[number]; }
    if (number == 2 || number == 3) { return 1; }
    if (number % 2 == 0 || number % 3 == 0 || number <= 1) { return 0; }

    int32 i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 get_number_of_distinct_prime_factors(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 0) { return 0; }

    int32 count = 0;
    int32 temp = number;
 
    for (int32 i = 0; i < NUM_PRIMES; ++i)
    {
        int32 prime = primes[i];
        if (prime * prime > number)
        { return count + 1; }

        if (temp % prime == 0)
        { ++count; }

        while (temp % prime == 0)
        { temp /= prime; }

        if (temp == 1)
        { break; }
    }

    return count;
}
