#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#define PRIME_CACHE_SIZE 1000000
#define NUM_PRIMES 78498

typedef int32_t int32;

/*
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 replace_nth_digit(int32 number, int32 n, int32 digit);
int32 get_num_digits(int32 number);

int32 primes[NUM_PRIMES];
int32 prime_cache[PRIME_CACHE_SIZE];
int32 prime_cache_filled = 0;

int main()
{
    printf("Project Euler 051 - Prime digit replacements\n"
            "https://projecteuler.net/problem=51");

    fill_prime_cache();

    int32 five_digit_replace[25*5] = 
    {
        1, 0, 0, 0, 0,
        0, 1, 0, 0, 0,
        0, 0, 1, 0, 0,
        0, 0, 0, 1, 0,
        0, 0, 0, 0, 1,
        1, 1, 0, 0, 0,
        1, 0, 1, 0, 0,
        1, 0, 0, 1, 0,
        1, 0, 0, 0, 1,
        0, 1, 1, 0, 0,
        0, 1, 0, 1, 0,
        0, 1, 0, 0, 1,
        0, 0, 1, 1, 0,
        0, 0, 1, 0, 1,
        0, 0, 0, 1, 1,
        1, 1, 1, 0, 0,
        1, 1, 0, 1, 0,
        1, 1, 0, 0, 1,
        1, 0, 1, 1, 0,
        1, 0, 1, 0, 1,
        1, 0, 0, 1, 1,
        0, 1, 1, 1, 0,
        0, 1, 1, 0, 1,
        0, 1, 0, 1, 1,
        0, 0, 1, 1, 1
    };

    int32 six_digit_replace[41*6] = 
    {
        1, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0,
        0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 1,
        1, 1, 0, 0, 0, 0,
        1, 0, 1, 0, 0, 0,
        1, 0, 0, 1, 0, 0,
        1, 0, 0, 0, 1, 0,
        1, 0, 0, 0, 0, 1,
        0, 1, 1, 0, 0, 0,
        0, 1, 0, 1, 0, 0,
        0, 1, 0, 0, 1, 0,
        0, 1, 0, 0, 0, 1,
        0, 0, 1, 1, 0, 0,
        0, 0, 1, 0, 1, 0,
        0, 0, 1, 0, 0, 1,
        0, 0, 0, 1, 1, 0,
        0, 0, 0, 1, 0, 1,
        0, 0, 0, 0, 1, 1,
        1, 1, 1, 0, 0, 0,
        1, 1, 0, 1, 0, 0,
        1, 1, 0, 0, 1, 0,
        1, 1, 0, 0, 0, 1,
        1, 0, 1, 1, 0, 0,
        1, 0, 1, 0, 1, 0,
        1, 0, 1, 0, 0, 1,
        1, 0, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1,
        1, 0, 0, 0, 1, 1,
        0, 1, 1, 1, 0, 0,
        0, 1, 1, 0, 1, 0,
        0, 1, 1, 0, 0, 1,
        0, 1, 0, 1, 1, 0,
        0, 1, 0, 1, 0, 1,
        0, 1, 0, 0, 1, 1,
        0, 0, 1, 1, 1, 0,
        0, 0, 1, 1, 0, 1,
        0, 0, 1, 0, 1, 1,
        0, 0, 0, 1, 1, 1,
    };

    int32 answer = 999999;
    int32 num_digits = 0;
    int32 digits[6] = { 0 };
    int32 family[8] = { 0 };

    // First five digit prime is at index 1229
    for (int32 i = 1229; i < NUM_PRIMES; ++i)
    {        
        // First six digit prime is at index 9592
        int32 prime = primes[i];
        num_digits = i >= 9592 ? 6 : 5;

        int32 temp = prime;
        for (int32 n = num_digits - 1; n >= 0; --n)
        {
            digits[n] = temp % 10;
            temp /= 10;
        }

        int32 iterations = num_digits == 5 ? 25 : 41;
        for (int32 j = 0; j < iterations; ++j)
        {
            int32 family_length = 0;
            for (int32 digit = 0; digit <= 9; ++digit)
            {
                int32 number = 0;
                int32 multiply = 1;
                int32 can_be_checked = 1;

                for (int32 k = num_digits - 1; k >= 0; --k)
                {
                    if (k == 0 && digit == 0)
                    {
                        can_be_checked = 0;
                        break;
                    }

                    if (num_digits == 5 && five_digit_replace[j * num_digits + k])
                    { number = number + digit * multiply; }
                    else if (num_digits == 6 && six_digit_replace[j * num_digits + k])
                    { number = number + digit * multiply; }
                    else
                    { number = number + digits[k] * multiply; }

                    multiply *= 10;
                }

                if (can_be_checked && is_prime(number))
                {
                    family[family_length] = number;
                    ++family_length;
                }
            }

            if (family_length == 8)
            {
                for (int32 l = 0; l < 8; ++l)
                {
                    if (family[l] < answer)
                    { answer = family[l]; }
                }

                goto end;
            }
        }
    }

    end:
    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }

    prime_cache_filled = 1;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (prime_cache_filled && number < PRIME_CACHE_SIZE) { return prime_cache[number]; }
    if (number == 2 || number == 3) { return 1; }
    if (number % 2 == 0 || number % 3 == 0 || number <= 1) { return 0; }

    int32 i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 replace_nth_digit(int32 number, int32 n, int32 digit)
{
    if (n < 0) { return number; }

    int32 result = 0;
    int32 multiply = 1;
    int32 flag = 1;

    int32 target = get_num_digits(number) - n + 1;
    int32 cur_digit = 0;

    while (number % 10 > 0)
    {
        ++cur_digit;
        int32 remainder = number % 10;

        if (flag && cur_digit == target)
        {
            flag = 0;
            result = result + digit * multiply;
        }
        else
        {
            result = result + remainder * multiply;
        }

        multiply *= 10;
        number = number / 10;
    }

    return result;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
