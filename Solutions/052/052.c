#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

typedef int32_t int32;

/*
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
*/

int32 do_numbers_have_same_digits(int32 a, int32 b);
int32 get_num_digits(int32 number);

int main()
{
    printf("Project Euler 052 - Permuted multiples\n"
            "https://projecteuler.net/problem=52");

    int32 answer = 0;
    int32 flag = 0;
    for (int32 i = 100000; ; ++i)
    {
        flag = 0;
        for (int32 j = 2; j <= 6; ++j)
        {
            if (!do_numbers_have_same_digits(i, i * j))
            { break; }

            if (j == 6) { flag = 1; }
        }

        if (flag)
        {
            answer = i;
            break;
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 do_numbers_have_same_digits(int32 a, int32 b)
{
    a = abs(a);
    b = abs(b);

    if (a == b)
    { return 1; }

    int32 num_digits = get_num_digits(a);
    if (num_digits != get_num_digits(b))
    { return 0; }

    int32 digits_a[10] = { 0 };
    int32 digits_b[10] = { 0 };

    for (int32 i = 0; i < num_digits; ++i)
    {
        ++digits_a[a % 10];
        ++digits_b[b % 10];
        a /= 10;
        b /= 10;
    }

    for (int32 i = 0; i < 10; ++i)
    {
        if (digits_a[i] != digits_b[i])
        { return 0; }
    }

    return 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
