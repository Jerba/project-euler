#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NUM_COMMON_WORDS 100

typedef int32_t int32;

/*
Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower case characters. Using p059_cipher.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.
*/

int32   xor_data(char* data, const char* key, const int32 data_len, const int32 key_len);
int32   xor_data_allow_only_common_characters(char* data, const char* key, const int32 data_len, const int32 key_len);
int32   find_number_of_common_words(const char* data, const int32 data_len);
int32   find_first_index_for_value(const char* data, const int32 data_len, char value);
char    find_most_common_value(const char* data, const int32 data_len);
char*   read_data(const char* path, int32* out_data_len);

static const char* common_words[NUM_COMMON_WORDS] = { "the", "be", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with", "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her", "she", "or", "an", "will", "my", "one", "all", "would", "there", "their", "what", "so", "up", "out", "if", "about", "who", "get", "which", "go", "me", "when", "make", "can", "like", "time", "no", "just", "him", "know", "take", "people", "into", "year", "your", "good", "some", "could", "them", "see", "other", "than", "then", "now", "look", "only", "come", "its", "over", "think", "also", "back", "after", "use", "two", "how", "our", "work", "first", "well", "way", "even", "new", "want", "because", "any", "these", "give", "day", "most", "us" };

int main()
{
    printf("Project Euler 059 - XOR decryption\n"
        "https://projecteuler.net/problem=59");

    int32 data_len = 0;
    char* data = read_data("p059_cipher.txt", &data_len);
    int32 data_size = data_len * sizeof(char);

    if (data == 0) { return 0; }

    // Most common character should be space (' ')
    char most_common = find_most_common_value(data, data_len);
    if (most_common < 0) {
        free(data);
        return 0;
    }

    // Finding index for ' '
    int32 first_index = find_first_index_for_value(data, data_len, most_common);
    if (first_index < 0) {
        free(data);
        return 0;
    }

    // Setting indexes for key
    int32 ki_a = 0, ki_b = 0, key_c = most_common % 3;
    if (key_c == 0) {
        ki_a = 1;
        ki_b = 2;
    }
    else if (key_c == 1) {
        ki_a = 0;
        ki_b = 2;
    }
    else {
        ki_a = 0;
        ki_b = 1;
    }

    // Finding out which lowercase character gives us ' '
    char pass_c = -1;
    for (char c = 'a'; c <= 'z'; ++c) {
        if ((most_common ^ c) == ' ') {
            pass_c = c;
            break;
        }
    }

    char key[] = "aaa";
    key[key_c] = pass_c;

    int32 best_match = 0;
    char correct_key[] = "aaa";
    correct_key[key_c] = pass_c;

    char* encrypted_data = (char*)malloc(data_size + sizeof(char));
    if (encrypted_data == 0) {
        free(data);
        return 0;
    }

    encrypted_data[data_len] = '\0';
    for (char a = 'a'; a <= 'z'; ++a) {
        for (char b = 'a'; b <= 'z'; ++b) {
            memcpy(encrypted_data, data, data_size);
            key[ki_a] = a;
            key[ki_b] = b;

            // Skip if XOR fails or the data contains letters we don't care about
            if (xor_data_allow_only_common_characters(encrypted_data, key, data_len, 3) != 0)
            { continue; }

            int32 num_common_words = find_number_of_common_words(encrypted_data, data_len);
            if (num_common_words > best_match) {
                best_match = num_common_words;
                correct_key[ki_a] = a;
                correct_key[ki_b] = b;
            }
        }
    }

    xor_data(data, correct_key, data_len, 3);

    int32 answer = 0;
    for (int32 i = 0; i < data_len; ++i)
    { answer += data[i]; }

    printf("\n\nDecrypted text with key '%s':\n%s", correct_key, data);

    free(data);
    free(encrypted_data);
    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");
    getchar();

    return 0;
}

int32 xor_data(char* data, const char* key, const int32 data_len, const int32 key_len)
{
    if (data == 0 || strlen(data) == 0 || data_len <= 0) { return 1; }
    if (key == 0 || strlen(key) == 0 || key_len <= 0) { return 2; }

    for (int32 i = 0; i < data_len; ++i) {
        data[i] = data[i] ^ key[i % key_len];
    }

    return 0;
}

int32 xor_data_allow_only_common_characters(char* data, const char* key, const int32 data_len, const int32 key_len)
{
    if (data == 0 || strlen(data) == 0 || data_len <= 0) { return 1; }
    if (key == 0 || strlen(key) == 0 || key_len <= 0) { return 2; }

    for (int32 i = 0; i < data_len; ++i) {
        data[i] = data[i] ^ key[i % key_len];
        if (data[i] < 32 || data[i] >= 126) {
            return 3;
        }
    }

    return 0;
}

int32 find_number_of_common_words(const char* data, const int32 data_len)
{
    if (data == 0 || strlen(data) == 0 || data_len <= 0) { return 0; }

    int32 size = sizeof(char) * data_len;
    char* lowercase = (char*)malloc(size + sizeof(char));

    if (lowercase == 0) { return 0; }

    lowercase[data_len] = '\0';
    memcpy(lowercase, data, size);

    for (int32 i = 0; i < data_len; ++i) {
        lowercase[i] = (char)tolower(lowercase[i]);
    }

    int32 total_num_words = 0;
    for (int32 i = 0; i < NUM_COMMON_WORDS; ++i) {
        char* temp = lowercase;
        while (temp = strstr(temp, common_words[i])) {
            ++total_num_words;
            ++temp;
        }
    }

    free(lowercase);
    lowercase = 0;

    return total_num_words;
}

int32 find_first_index_for_value(const char* data, const int32 data_len, char value)
{
    if (data == 0) { return -1; }
    if (data_len <= 0) { return -2; }

    for (int32 i = 0; i < data_len; ++i)
    {
        if (data[i] == value)
        { return i; }
    }

    return -3;
}

char find_most_common_value(const char* data, const int32 data_len)
{
    if (data == 0) { return -1; }
    if (data_len <= 0) { return -2; }

    char chars[128] = { 0 };
    char most_common = 0;
    for (int32 i = 0; i < data_len; ++i)
    {
        char c = data[i];
        if (c < 0 || c > 127) { continue; }

        ++chars[c];
        if (chars[c] > most_common) {
            most_common = c;
        }
    }

    return most_common;
}

char* read_data(const char* path, int32* out_data_len)
{
    FILE* fp;

    fp = fopen(path, "r");
    if (fp == 0) {
        printf("\nCould not open file %s", path);
        return 0;
    }

    fseek(fp, 0L, SEEK_END);
    int32 size = ftell(fp);
    rewind(fp);

    if (size <= 0) {
        fclose(fp);
        return 0;
    }

    char* str = (char*)malloc(sizeof(char) * size);
    if (str == 0) {
        fclose(fp);
        return 0;
    }

    fread(str, sizeof(char), size, fp);
    int32 byte_count = 1;

    for (int32 i = 0; i < size; ++i) {
        if (str[i] == ',') {
            ++byte_count;
        }
    }

    if (byte_count == 1) { goto error; }

    char* data = (char*)malloc(sizeof(char) * (byte_count + 1));
    if (data == 0) { goto error; }

    data[byte_count] = '\0';
    char* byte = strtok(str, ",\n");
    for (int32 i = 0; i < byte_count; ++i) {
        if (byte == 0) { break; }
        data[i] = (char)atoi(byte);
        byte = strtok(0, ",\n");
    }

    *out_data_len = byte_count;

    free(str);
    fclose(fp);

    return data;

    error:
    free(str);
    fclose(fp);
    return 0;
}
