#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef int32_t int32;

/*
NOTE: This problem is a more challenging version of Problem 81.

The minimal path sum in the 5 by 5 matrix below, by starting in any cell in the left column and finishing in any cell in the right column, and only moving up, down, and right, is indicated in red and bold; the sum is equal to 994.

|   131   673  +234  +103   +18   |
|  +201   +96  +342   965   150   |
|   630   803   746   422   111   |
|   537   699   497   121   956   |
|   805   732   524    37   331   |

Find the minimal path sum from the left column to the right column in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing an 80 by 80 matrix.
*/

int32 get_grid_index(int32 x, int32 y);
int32 read_grid_file(const char *path, int32 *grid);

const int32 grid_width = 80;
const int32 grid_size = 80 * 80;

int main()
{
    printf("Project Euler 082 - Path sum: three ways\n"
            "https://projecteuler.net/problem=82");

    int32 *grid = (int32*)malloc(sizeof(int32) * grid_size);
    int32 *temp = (int32*)malloc(sizeof(int32) * grid_width);

    if (read_grid_file("p082_matrix.txt", grid) != 0)
    {
        free(grid);
        free(temp);
        return 0;
    }

    for (int32 i = 0; i < grid_width; ++i)
    {
        int32 gi = get_grid_index(grid_width - 1, i);
        temp[i] = grid[gi];
    }

    for (int32 i = grid_width - 2; i >= 0; --i)
    {
        temp[0] += grid[get_grid_index(i, 0)];

        for (int32 j = 1; j < grid_width; ++j)
        {
            int32 gi = get_grid_index(i, j);
            int32 a = temp[j - 1] + grid[gi];
            int32 b = temp[j] + grid[gi];
            temp[j] = a < b ? a : b;
        }

        for (int32 j = grid_width - 2; j >= 0; --j)
        {
            int32 gi = get_grid_index(i, j);
            int32 a = temp[j];
            int32 b = temp[j + 1] + grid[gi];
            temp[j] = a < b ? a : b;
        }
    }

    int32 answer = 9999999;
    for (int32 i = 0; i < grid_width; ++i)
    {
        if (temp[i] < answer)
        { answer = temp[i]; }
    }

    free(grid);
    free(temp);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_grid_index(int32 x, int32 y)
{
    return y * grid_width + x;
}

int32 read_grid_file(const char *path, int32 *grid)
{
    FILE *fp;

    fp = fopen(path, "r");
    if (fp == 0)
    {
        printf("\nCould not open file %s", path);
        return 1;
    }

    fseek(fp, 0L, SEEK_END);
    int32 size = ftell(fp);
    rewind(fp);

    if (size <= 0)
    {
        fclose(fp);
        return 2;
    }

    char *str = (char*)malloc(sizeof(char) * size);
    fread(str, sizeof(char), size, fp);

    char *value_str;
    int32 i = 0;

    for (value_str = strtok(str, ",\n"); value_str != 0; value_str = strtok(0, ",\n"))
    {
        grid[i] = atoi(value_str);
        ++i;
    }

    free(str);
    fclose(fp);
    return 0;
}
