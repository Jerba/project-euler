#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define FACTORIAL_CACHE_SIZE 10
#define UPPER_LIMIT 2540160

typedef uint64_t uint64;

/*
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
*/

uint64 factorial_cache[FACTORIAL_CACHE_SIZE];
uint64 get_num_digits(uint64 number);
uint64 get_factorial(uint64 number);

int main()
{
    printf("Project Euler 034 - Digit factorials\n"
            "https://projecteuler.net/problem=34");

    for (uint64 i = 0; i < FACTORIAL_CACHE_SIZE; ++i)
    {
        factorial_cache[i] = get_factorial(i);
    }

    uint64 answer = 0;
    for (uint64 i = 3; i <= UPPER_LIMIT; ++i)
    {
        uint64 sum = 0;
        uint64 temp = i;
        uint64 num_digits = get_num_digits(i);

        for (uint64 j = 0; j < num_digits; ++j)
        {
            uint64 digit = temp % 10;
            temp /= 10;
            sum += factorial_cache[digit];
        }

        if (sum == i)
        { answer += sum; }
    }

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

uint64 get_num_digits(uint64 number)
{
    if (number == 0) { return 1; }
    return (uint64)floor(log10((double)number)) + 1;
}

uint64 get_factorial(uint64 number)
{
    if (number == 0)
    { return 1; }

    uint64 factorial = number;

    for (uint64 i = 1; i < number; ++i)
    {
        factorial *= i;
    }

    return factorial;
}
