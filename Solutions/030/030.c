#include <stdio.h>
#include <stdint.h>
#include <math.h>

typedef int32_t int32;

/*
Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

    1634 = 1^4 + 6^4 + 3^4 + 4^4
    8208 = 8^4 + 2^4 + 0^4 + 8^4
    9474 = 9^4 + 4^4 + 7^4 + 4^4

As 1 = 14 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
*/

int32 get_num_digits(double number);
double find_upper_limit(double power);

int main()
{
    printf("Project Euler 030 - Digit fifth powers\n"
            "https://projecteuler.net/problem=30");

    const double power = 5;
    double upper_limit = find_upper_limit(power);
    double answer = 0;

    for (double start = 2; start <= upper_limit; ++start)
    {
        double sum = 0;
        int32 num_digits = get_num_digits(start);
        int32 temp = (int32)start;

        for (int32 i = 0; i < num_digits; ++i)
        {
            double digit = (double)(temp % 10);
            temp /= 10;

            sum += pow(digit, power);
        }

        if (sum == start)
        {
            answer += sum;
            // printf("\nNumber %0.0f can be written as the sum of fifth powers of its digits.", sum);
        }
    }

    printf("\n\nAnswer: %0.0f", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_num_digits(double number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}

double find_upper_limit(double power)
{
    double upper_limit = pow(9, power);

    int32 num_digits = get_num_digits(upper_limit);
    upper_limit = num_digits * pow(9, power);

    num_digits = get_num_digits(upper_limit);
    upper_limit = num_digits * pow(9, power);

    return upper_limit;
}
