#include <stdio.h>
#include <stdint.h>

#define NUM_CHECKS 1000

typedef int32_t int32;

/*
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

    1/2	= 	0.5
    1/3	= 	0.(3)
    1/4	= 	0.25
    1/5	= 	0.2
    1/6	= 	0.1(6)
    1/7	= 	0.(142857)
    1/8	= 	0.125
    1/9	= 	0.(1)
    1/10	= 	0.1 

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
*/

int32 get_recurring_cycle_length(int32 d);

int main()
{
    printf("Project Euler 026 - Reciprocal cycles\n"
            "https://projecteuler.net/problem=26");

    int32 answer = 0;
    int32 cycle_length = 0;

    for (int32 d = 2; d < NUM_CHECKS; ++d)
    {
        int32 temp = get_recurring_cycle_length(d);

        if (temp > cycle_length)
        {
            cycle_length = temp;
            answer = d;
        }

        // printf("\nRecurring cycle length for 1 / %d is %d", d, temp);
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_recurring_cycle_length(int32 d)
{
    if (d < 0)  { d *= -1; }
    if (d <= 1) { return 0; }
    if (d == 2) { return 1; }

    int32 n = 1;

    while (n < d)
    {
        n *= 10;
    }

    int32 i = 0;
    int32 cycle_length = 0;
    int32 adjusts = 0;
    int32 remainder = n % d;

    for (i = 0; i < d; ++i)
    {
        ++cycle_length;
        adjusts = 0;

        while (remainder < d)
        {
            ++adjusts;
            if (remainder == 0)
            { remainder = 10; }
            else
            { remainder *= 10; }
        }

        if (adjusts > 1)
        { cycle_length += adjusts - 1; }

        if (n == remainder)
        { return cycle_length; }

        remainder = remainder % d;
    }

    int32 temp = d;
    while (temp % 2 == 0) { temp /= 2; }
    while (temp % 5 == 0) { temp /= 5; }

    return get_recurring_cycle_length(temp);
}
