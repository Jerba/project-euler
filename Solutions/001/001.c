#include <stdio.h>

int main()
{
    printf("Project Euler 001 - Multiples of 3 and 5\n"
            "https://projecteuler.net/problem=1\n\n");

    int answer = 0;

    printf("Multiples of 3 or 5: ");
    for (int i = 3; i < 1000; ++i)
    {
        if (i % 3 == 0 || i % 5 == 0)
        {
            answer += i;
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}