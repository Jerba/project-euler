#include <stdio.h>
#include <stdint.h>

#define GRID_WIDTH 1001

typedef int32_t int32;

/*
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
*/

int32 fill_grid();
int32 get_grid_index(int32 x, int32 y);

int32 grid[GRID_WIDTH * GRID_WIDTH] = {0};

int main()
{
    printf("Project Euler 028 - Number spiral diagonals\n"
            "https://projecteuler.net/problem=28");

    if (fill_grid() != 0)
    { return 0; }

    int32 answer = grid[get_grid_index(GRID_WIDTH / 2, GRID_WIDTH / 2)];
    for (int32 i = 0; i < GRID_WIDTH; ++i)
    {
        if (i != GRID_WIDTH / 2)
        {
            answer += grid[get_grid_index(i, i)];
            answer += grid[get_grid_index(GRID_WIDTH - 1 - i, i)];
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 fill_grid()
{
    if (GRID_WIDTH % 2 == 0)
    {
        printf("\nGRID_WIDTH must be odd value.");
        return 1;
    }

    int32 value = GRID_WIDTH * GRID_WIDTH;
    int32 row, col;

    for (int32 round = 1; round <= GRID_WIDTH / 2 + 1; ++round)
    {
        for (col = GRID_WIDTH - round; col >= round - 1; --col)
        {
            grid[get_grid_index(col, round - 1)] = value;
            --value;
        }

        for (row = round; row <= GRID_WIDTH - round; ++row)
        {
            grid[get_grid_index(round - 1, row)] = value;
            --value;
        }

        for (col = round; col <= GRID_WIDTH - round; ++col)
        {
            grid[get_grid_index(col, GRID_WIDTH - round)] = value;
            --value;
        }

        for (row = GRID_WIDTH - round - 1; row >= round; --row)
        {
            grid[get_grid_index(GRID_WIDTH - round, row)] = value;
            --value;
        }
    }

    // printf("\n\nGrid:\n");
    // for (row = 0; row < GRID_WIDTH; ++row)
    // {
        // for (col = 0; col < GRID_WIDTH; ++col)
        // {
            // printf("|%04d|", grid[get_grid_index(col, row)]);
        // }
        // printf("\n");
    // }

    return 0;
}

int32 get_grid_index(int32 x, int32 y)
{
    return y * GRID_WIDTH + x;
}