#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define BUFFER_SIZE 16
#define NUM_SUDOKUS 50
#define PRINT_SOLUTIONS 0

typedef int32_t int32;

typedef struct sudoku_t
{
    int32 grid[9][9];
} sudoku_t;

/*
Su Doku (Japanese meaning number place) is the name given to a popular puzzle concept. Its origin is unclear, but credit must be attributed to Leonhard Euler who invented a similar, and much more difficult, puzzle idea called Latin Squares. The objective of Su Doku puzzles, however, is to replace the blanks (or zeros) in a 9 by 9 grid in such that each row, column, and 3 by 3 box contains each of the digits 1 to 9. Below is an example of a typical starting puzzle grid and its solution grid.

A well constructed Su Doku puzzle has a unique solution and can be solved by logic, although it may be necessary to employ "guess and test" methods in order to eliminate options (there is much contested opinion over this). The complexity of the search determines the difficulty of the puzzle; the example above is considered easy because it can be solved by straight forward direct deduction.

By solving all fifty puzzles find the sum of the 3-digit numbers found in the top left corner of each solution grid; for example, 483 is the 3-digit number found in the top left corner of the solution grid above.
*/

void solve_sudoku(sudoku_t *sudoku);
bool fill_numbers(int32 sudoku[9][9], int32 y, int32 x);
bool is_number_used(int32 sudoku[9][9], int32 y, int32 x, int32 number);
int32 read_sudoku_file();

sudoku_t sudokus[NUM_SUDOKUS];

int32 main()
{
    printf("Project Euler 096 - Su Doku\n"
            "https://projecteuler.net/problem=96");

    if (read_sudoku_file() != 0)
        return 0;

    int32 answer = 0;
    for (int i = 0; i < NUM_SUDOKUS; ++i)
    {
        solve_sudoku(&sudokus[i]);

        int32 topLeftDigits = 0;

        for (int32 j = 0; j < 3; ++j)
        {
            int32 temp = sudokus[i].grid[0][j];
            topLeftDigits = topLeftDigits * 10 + temp;
        }

        answer += topLeftDigits;
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void solve_sudoku(sudoku_t *sudoku)
{
    if (!sudoku)
        return;

    if (!fill_numbers(sudoku->grid, 0, 0))
    {
        printf("\n\nFailed to find solution.");
        return;
    }

    if (PRINT_SOLUTIONS)
    {
        printf("\n\nSolution:\n");
        for (int32 y = 0; y < 9; ++y)
        {
            for (int32 x = 0; x < 9; ++x)
            {
                printf(" %d ", sudoku->grid[y][x]);
                if (x == 2 || x == 5)
                    printf("|");
            }

            if (y == 2 || y == 5)
                printf("\n---------+---------+---------\n");
            else
                printf("\n");
        }
    }
}

bool fill_numbers(int32 sudoku[9][9], int32 y, int32 x)
{
    if (y >= 9 || x >= 9)
        return true;

    if (sudoku[y][x] != 0)
    {
        if (x + 1 < 9)
            return fill_numbers(sudoku, y, x + 1);
        else if (y + 1 < 9)
            return fill_numbers(sudoku, y + 1, 0);

        return true;
    }

    for (int32 candidate = 1; candidate <= 9; ++candidate)
    {
        if (!is_number_used(sudoku, y, x, candidate))
        {
            sudoku[y][x] = candidate;

            if (x + 1 < 9)
            {
                if (fill_numbers(sudoku, y, x + 1))
                    return true;
            }
            else if (y + 1 < 9)
            {
                if (fill_numbers(sudoku, y + 1, 0))
                    return true;
            }
            else
                return true;

            sudoku[y][x] = 0;
        }
    }

    return false;
}


bool is_number_used(int32 sudoku[9][9], int32 y, int32 x, int32 number)
{
    int32 squareY = (y / 3) * 3;
    int32 squareX = (x / 3) * 3;

    for (int32 i = 0; i < 9; ++i)
    {
        if (sudoku[y][i] == number)
            return true;
        if (sudoku[i][x] == number)
            return true;
        if (sudoku[squareY + (i % 3)][squareX + (i / 3)] == number)
            return true;
    }

    return false;
}

int32 read_sudoku_file()
{
    FILE *fp;
    char str[BUFFER_SIZE];

    fp = fopen("p096_sudoku.txt", "r");
    if (fp == 0)
    {
        printf("\n\nCould not open file p096_sudoku.txt");
        return 1;
    }

    int32 si = 0;
    while (fgets(str, BUFFER_SIZE, fp) != NULL)
    {
        for (int32 i = 0; i < 9; ++i)
        {
            fgets(str, BUFFER_SIZE, fp);
            for (int32 j = 0; j < 9; ++j)
                sudokus[si].grid[i][j] = str[j] - '0';
        }

        ++si;
    }

    fclose(fp);
    return 0;
}
