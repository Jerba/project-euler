#include <stdio.h>
#include <stdint.h>

#define NUM_PRIMES 78498
#define MAX_NUMBER 1000000

typedef int32_t int32;

/*
The prime 41, can be written as the sum of six consecutive primes:
41 = 2 + 3 + 5 + 7 + 11 + 13

This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
*/

void fill_primes();
int32 is_prime(int32 number);

int32 primes[NUM_PRIMES];
int32 prime_hash[MAX_NUMBER] = {0};

int main()
{
    printf("Project Euler 050 - Consecutive prime sum\n"
            "https://projecteuler.net/problem=50");

    fill_primes();

    int32 answer = 0;
    int32 best_streak = 0;

    int32 loop = 1;
    int32 i = 0, j = 0;

    while (loop)
    {
        int32 sum = 0;
        int32 streak = 0;
        for (j = i + 1; j < NUM_PRIMES; ++j)
        {
            ++streak;
            sum += primes[j];

            if (sum >= MAX_NUMBER)
            {
                if (streak < best_streak)
                { loop = 0; }

                break;
            }

            if (prime_hash[sum] && streak > 1)
            {
                // printf("\n\nPrime %d can be written as sum of %d consecutive primes.", sum, streak);

                if (streak > best_streak)
                {
                    answer = sum;
                    best_streak = streak;
                }
            }
        }

        ++i;
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_primes()
{
    for (int32 i = 0, j = 0; i < MAX_NUMBER; ++i)
    {
        if (is_prime(i))
        {
            prime_hash[i] = 1;
            primes[j] = i;
            ++j;
        }
    }
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return 1; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return 0; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}
