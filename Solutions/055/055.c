#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define MAX_DIGITS 32

typedef int32_t int32;

/*
If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.

Not all numbers produce palindromes so quickly. For example,

349 + 943 = 1292,
1292 + 2921 = 4213
4213 + 3124 = 7337

That is, 349 took three iterations to arrive at a palindrome.

Although no one has proved it yet, it is thought that some numbers, like 196, never produce a palindrome.
A number that never forms a palindrome through the reverse and add process is called a Lychrel number.
Due to the theoretical nature of these numbers, and for the purpose of this problem, we shall assume that a number is Lychrel until proven otherwise.
In addition you are given that for every number below ten-thousand, it will either (i) become a palindrome in less than fifty iterations, or, (ii) no one, with all the computing power that exists, has managed so far to map it to a palindrome. In fact, 10677 is the first number to be shown to require over fifty iterations before producing a palindrome: 4668731596684224866951378664 (53 iterations, 28-digits).

Surprisingly, there are palindromic numbers that are themselves Lychrel numbers; the first example is 4994.

How many Lychrel numbers are there below ten-thousand?

NOTE: Wording was modified slightly on 24 April 2007 to emphasise the theoretical nature of Lychrel numbers.
*/

int32 add_digits(int32 sum[MAX_DIGITS], int32 a[MAX_DIGITS], int32 b[MAX_DIGITS]);
void init_digits(int32 digits[MAX_DIGITS], int32 number);
void reverse_digits(int32 digits[MAX_DIGITS]);
int32 find_start_index(int32 digits[MAX_DIGITS]);
int32 is_palindrome(int32 digits[MAX_DIGITS]);

int main()
{
    printf("Project Euler 055 - Lychrel numbers\n"
            "https://projecteuler.net/problem=55");

    int32 sum[MAX_DIGITS]   = { 0 };
    int32 a[MAX_DIGITS]     = { 0 };
    int32 b[MAX_DIGITS]     = { 0 };
    int32 answer = 0;

    for (int32 i = 1; i < 10000; ++i)
    {
        int32 flag = 1;
        init_digits(a, i);

        for (int32 j = 0; j < 50; ++j)
        {
            memcpy(b, a, sizeof(int32) * MAX_DIGITS);
            reverse_digits(b);
            add_digits(sum, a, b);

            if (is_palindrome(sum))
            {
                flag = 0;
                break;
            }

            memcpy(a, sum, sizeof(int32) * MAX_DIGITS);
        }

        if (flag) { ++answer; }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 add_digits(int32 sum[MAX_DIGITS], int32 a[MAX_DIGITS], int32 b[MAX_DIGITS])
{
    memset(sum, 0, sizeof(int32) * MAX_DIGITS);

    int32 start_a = find_start_index(a);
    int32 start_b = find_start_index(b);
    int32 start = start_a < start_b ? start_a : start_b;

    for (int32 i = MAX_DIGITS - 1; i >= start; --i)
    {
        sum[i] += a[i] + b[i];
        if (sum[i] >= 10)
        {
            if (i - 1 < 0)
            { return 1; }

            sum[i] -= 10;
            sum[i - 1] += 1;
        }
    }

    return 0;
}

void init_digits(int32 digits[MAX_DIGITS], int32 number)
{
    if (number < 0) { number *= -1; }

    memset(digits, 0, sizeof(int32) * MAX_DIGITS);

    int32 index = MAX_DIGITS - 1;
    while (number != 0)
    {
        int32 digit = number % 10;
        number /= 10;
        digits[index] = digit;
        index--;
    }
}

void reverse_digits(int32 digits[MAX_DIGITS])
{
    int32 start = find_start_index(digits);
    int32 i, j;
    for (i = start, j = 0; j < ((MAX_DIGITS - start) / 2); ++i, ++j)
    {
        int32 temp = digits[i];
        digits[i] = digits[MAX_DIGITS - j - 1];
        digits[MAX_DIGITS - j - 1] = temp;
    }
}

int32 find_start_index(int32 digits[MAX_DIGITS])
{
    for (int32 i = 0; i < MAX_DIGITS; ++i)
    {
        if (digits[i] != 0)
        { return i; }
    }

    return MAX_DIGITS - 1;
}

int32 is_palindrome(int32 digits[MAX_DIGITS])
{
    int32 start = find_start_index(digits);
    int32 middle = start + ((MAX_DIGITS - start) / 2);

    for (int32 i = start, j = 0; i <= middle; ++i, ++j)
    {
        if (digits[i] != digits[MAX_DIGITS - j - 1])
        { return 0; }
    }

    return 1;
}
