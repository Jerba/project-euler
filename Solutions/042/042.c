#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef int32_t int32;

/*
The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?
*/

int32 read_and_count_triangle_words(const char *path);

int32 triangle_number_cache[1000] = {0};

int main()
{
    printf("Project Euler 042 - Coded triangle numbers\n"
            "https://projecteuler.net/problem=42");

    int32 triangle_number = 1;
    for (int32 i = 2; triangle_number < 1000; ++i)
    {
        triangle_number_cache[triangle_number] = 1;
        triangle_number += i;
    }

    int32 answer = read_and_count_triangle_words("p042_words.txt");
    if (answer < 0)
    {
        printf("\n\nFailed to open or read the file.");
        getchar();
        return 0;
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 read_and_count_triangle_words(const char *path)
{
    FILE *fp;

    fp = fopen(path, "r");
    if (fp == 0)
    {
        printf("\nCould not open file %s", path);
        return -1;
    }

    fseek(fp, 0L, SEEK_END);
    int32 size = ftell(fp);
    rewind(fp);

    if (size <= 0)
    {
        fclose(fp);
        return -2;
    }

    char *str = (char*)malloc(sizeof(char) * size);
    fread(str, sizeof(char), size, fp);

    int32 triangle_word_count = 0;

    for (char *word = strtok(str, ",\n"); word != 0; word = strtok(0, ",\n"))
    {
        int32 word_value = 0;
        for (int32 i = 0; i < strlen(word); ++i)
        {
            char c = word[i];
            word_value += (c >= 'A' && c <= 'Z') ? (c - 'A' + 1) : 0;
        }

        if (triangle_number_cache[word_value])
        {
            // printf("\nWord %s is a triangle word with value %d", word, word_value);
            ++triangle_word_count;
        }
    }

    free(str);
    fclose(fp);

    return triangle_word_count;
}
