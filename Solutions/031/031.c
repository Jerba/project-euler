#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define NUM_COINS 8

typedef int32_t int32;

/*
In the United Kingdom the currency is made up of pound (£) and pence (p). There are eight coins in general circulation:
    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p).

It is possible to make £2 in the following way:
    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p

How many different ways can £2 be made using any number of coins?
*/

int32 get_number_of_different_ways(int32 money);

int32 coins[] = { 1, 2, 5, 10, 20, 50, 100, 200 };

int main()
{
    printf("Project Euler 031 - Coin sums\n"
            "https://projecteuler.net/problem=31");

    int32 answer = get_number_of_different_ways(200);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_number_of_different_ways(int32 money)
{
    if (money <= 0) { return 0; }

    int32 size = money + 1;

    int32 *different_ways = (int32*)malloc(sizeof(int32) * size);
    memset(different_ways, 0, sizeof(int32) * size);
    different_ways[0] = 1;

    for (int32 i = 0; i < NUM_COINS; ++i)
    {
        int32 coin = coins[i];
        for (int32 j = coin; j <= money; ++j)
        { different_ways[j] += different_ways[j - coin]; }
    }

    int32 num_ways = different_ways[money];
    free(different_ways);

    return num_ways;
}
