#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define MAX_ABUNDANTS 28123
#define MAX_SUMS MAX_ABUNDANTS + 1

typedef int32_t int32;

/*
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

int32 get_answer();
void find_abundants();
int32 is_number_abundant(int32 number);

int32 abundant_numbers[MAX_ABUNDANTS];
int32 num_abundants = 0;

int main()
{
    printf("Project Euler 023 - Non-abundant sums\n"
            "https://projecteuler.net/problem=23");

    find_abundants();
    int32 answer = get_answer();

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 get_answer()
{
    if (num_abundants <= 0) { return 0; }

    int32 i, j;
    int32 sum = 0;

    int32 abundant_sums[MAX_SUMS] = {0};

    for (i = 0; i < num_abundants; ++i)
    {
        for (j = i; j < num_abundants; ++j)
        {
            int32 temp = abundant_numbers[i] + abundant_numbers[j];

            if (temp < MAX_SUMS)
            { abundant_sums[temp] = temp; }
            else
            { break; }
        }
    }

    for (i = 0; i < MAX_SUMS; ++i)
    {
        if (abundant_sums[i] == 0)
        { sum += i; }
    }

    return sum;
}

void find_abundants()
{
    for (int32 i = 0; i < MAX_ABUNDANTS; ++i)
    {
        if (is_number_abundant(i))
        {
            abundant_numbers[num_abundants] = i;
            ++num_abundants;
        }
    }
}

int32 is_number_abundant(int32 number)
{
    int32 sum = 0;
    int32 squareroot = (int32)sqrt(number);

    for (int32 i = 1; i <= squareroot; ++i)
    {
        if (number % i == 0)
        {
            if (number / i == i)
            {
                sum = sum + i;
            }
            else
            {
                sum += i;
                sum += (number / i);
            }
        }
    }

    return (sum - number) > number;
}
