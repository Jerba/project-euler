#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_DIGITS 4096

typedef int32_t int32;
typedef uint64_t uint64;

/*
The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
*/

uint64 simple_solution();
uint64 brute_solution();
int32 addition(int32 *sum, int32 *a, int32 *b);
int32 multiply(int32 *value, int32 factor);
int32 find_start_index(int32 *number);
int32 get_num_digits(int32 number);

static const int32 ARRAY_SIZE = sizeof(int32) * MAX_DIGITS;

int32 main()
{
    printf("Project Euler 048 - Self powers\n"
            "https://projecteuler.net/problem=48");

    uint64 answer = simple_solution();

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

uint64 simple_solution()
{
    uint64 answer = 0;
    uint64 modulo = 10000000000;

    for (uint64 i = 1; i <= 1000; ++i)
    {
        uint64 temp = i;
        for (uint64 j = 1; j < i; ++j)
        {
            temp *= i;
            temp %= modulo;
        }

        answer += temp;        
    }

    answer %= modulo;
    return answer;
}

uint64 brute_solution()
{
    int32 *answer_array = (int32*)malloc(ARRAY_SIZE);
    int32 *multiply_array = (int32*)malloc(ARRAY_SIZE);
    int32 *temp_array = (int32*)malloc(ARRAY_SIZE);
    memset(answer_array, 0, ARRAY_SIZE);
    memset(multiply_array, 0, ARRAY_SIZE);
    memset(temp_array, 0, ARRAY_SIZE);

    for (int32 number = 1000; number > 0; --number)
    {
        memset(multiply_array, 0, ARRAY_SIZE);
        int32 num_digits = get_num_digits(number);
        int32 temp = number;

        for (int32 i = 0; i < num_digits; ++i)
        {
            int32 digit = temp % 10;
            temp /= 10;

            multiply_array[MAX_DIGITS - 1 - i] = digit;
        }

        for (int32 i = 1; i < number; ++i)
        { multiply(multiply_array, number); }

        memcpy(temp_array, answer_array, ARRAY_SIZE);
        addition(answer_array, multiply_array, temp_array);
    }

    uint64 answer = 0;
    uint64 adj = 1;
    for (int32 i = MAX_DIGITS - 1; i >= MAX_DIGITS - 10; --i)
    {
        answer += answer_array[i] * adj;
        adj *= 10;
    }

    free(answer_array);
    free(multiply_array);
    free(temp_array);

    return answer;
}

int32 addition(int32 *sum, int32 *a, int32 *b)
{
    memset(sum, 0, ARRAY_SIZE);

    int32 start_a = find_start_index(a);
    int32 start_b = find_start_index(b);
    int32 start = start_a < start_b ? start_a : start_b;

    for (int32 i = MAX_DIGITS - 1; i >= start; --i)
    {
        sum[i] += a[i] + b[i];
        if (sum[i] >= 10)
        {
            if (i - 1 < 0)
            { return 1; }

            sum[i] -= 10;
            sum[i - 1] += 1;
        }
    }

    return 0;
}

int32 multiply(int32 *number, int32 factor)
{
    if (factor < 0) { factor *= -1; }
    if (factor == 1) { return 0; }

    if (factor == 0)
    {
        memset(number, 0, ARRAY_SIZE);
        return 0;
    }

    int32 original_number[MAX_DIGITS] = {0};
    memcpy(original_number, number, ARRAY_SIZE);
    memset(number, 0, ARRAY_SIZE);

    int32 i = 0, j = 0, carry = 0;

    int32 start_index   = find_start_index(original_number);
    int32 end_index     = MAX_DIGITS - 1;
    int32 num_digits    = get_num_digits(factor);
    int32 temp          = factor;

    int32 temp_value[MAX_DIGITS] = {0};
    for (i = 0; i < num_digits; ++i)
    {
        memcpy(temp_value, original_number, ARRAY_SIZE);

        int32 digit = temp % 10;
        temp /= 10;

        for (j = end_index; j >= start_index; --j)
        {
            int32 val = (original_number[j] * digit) + carry;

            carry = val / 10;
            val -= 10 * carry;
            temp_value[j] = val;
        }

        int32 index = start_index;

        if (carry > 0)
        {
            if (start_index - 1 < 0) { goto error; }
            --index;

            temp_value[index] = carry;
            carry = 0;
        }

        for (j = end_index; j >= index; --j)
        {
            if (j - i - 1 < 0) { goto error; }

            int32 temp_val = number[j - i] + temp_value[j];
            int32 temp_carry = temp_val / 10;
            temp_val -= 10 * temp_carry;

            number[j - i] = temp_val;
            number[j - i - 1] += temp_carry;
        }
    }

    start_index = find_start_index(number);

    return 0;

    error:
    memcpy(number, original_number, ARRAY_SIZE);
    printf("\n\nError: Number was too big to calculate.");

    return 1;
}

int32 find_start_index(int32 *number)
{
    for (int32 i = 0; i < MAX_DIGITS; ++i)
    {
        if (number[i] != 0)
        {
            return i;
        }
    }

    return MAX_DIGITS - 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}
