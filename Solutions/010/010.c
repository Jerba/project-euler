#include <stdio.h>

#ifndef bool
#define bool int
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
*/

bool is_prime(int number);

int main()
{
    printf("Project Euler 010 - Summation of primes\n"
            "https://projecteuler.net/problem=10");

    unsigned long long answer = 0;

    for (int i = 2; i < 2000000; ++i)
    {
        if (is_prime(i))
        { answer += i; }
    }

    printf("\n\nAnswer: %lli", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

bool is_prime(int number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return true; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return false; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return false; }

        i += 6;
    }

    return true;
}