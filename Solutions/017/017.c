#include <stdio.h>

/*
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
*/

int get_letter_count(int number);

int main()
{
    printf("Project Euler 017 - Number letter counts\n"
            "https://projecteuler.net/problem=17");

    int answer = 0;
    for (int i = 1; i <= 1000; ++i)
    {
        answer += get_letter_count(i);
    }

    printf("\n\nAnswer: %i", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int get_letter_count(int number)
{
    if (number < 0 || number > 1000) { return 0; }
    if (number == 1000) { return 11; }

    int count = 0;
    if (number >= 900) { count += 11; }
    else if (number >= 700) { count += 12; }
    else if (number >= 600) { count += 10; }
    else if (number >= 400) { count += 11; }
    else if (number >= 300) { count += 12; }
    else if (number >= 100) { count += 10; }

    if(number % 100 == 0)
    { return count; }

    if (number > 100)
    {
        count += 3; // and

        while (number > 100)
        { number -= 100; }
    }

    if (number >= 80) { count += 6; }
    else if (number >= 70) { count += 7; }
    else if (number >= 40) { count += 5; }
    else if (number >= 20) { count += 6; }
    else if (number >= 10)
    {
        switch (number)
        {
            case 10: count += 3; break;
            case 11: case 12: count += 6; break;
            case 13: case 14: case 18: case 19: count += 8; break;
            case 15: case 16: count += 7; break;
            case 17: count += 9; break;
        }

        return count;
    }

    while (number >= 10)
    { number -= 10; }

    switch (number)
    {
        case 1: case 2: case 6: count += 3; break;
        case 3: case 7: case 8: count += 5; break;
        case 4: case 5: case 9: count += 4; break;
    }

    return count;
}
