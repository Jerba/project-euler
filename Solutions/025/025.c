#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define MAX_DIGITS 1000

typedef int32_t int32;

/*
The Fibonacci sequence is defined by the recurrence relation:
    Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.

Hence the first 12 terms will be:
    F1 = 1
    F2 = 1
    F3 = 2
    F4 = 3
    F5 = 5
    F6 = 8
    F7 = 13
    F8 = 21
    F9 = 34
    F10 = 55
    F11 = 89
    F12 = 144

The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
*/

int32 addition(int32 sum[MAX_DIGITS], int32 a[MAX_DIGITS], int32 b[MAX_DIGITS]);
int32 find_start_index(int32 number[MAX_DIGITS]);

int main()
{
    printf("Project Euler 025 - 1000-digit Fibonacci number\n"
            "https://projecteuler.net/problem=25");

    int32 answer = 3;
    int32 f1[MAX_DIGITS] = {0};
    int32 f2[MAX_DIGITS] = {0};
    int32 ftemp[MAX_DIGITS] = {0};

    f1[MAX_DIGITS - 1] = 1;
    f2[MAX_DIGITS - 1] = 2;

    while (1)
    {
        ++answer;
        if (addition(ftemp, f1, f2) != 0)
        {
            printf("\n\nFailed to add values together");
            break;
        }

        int32 start = find_start_index(ftemp);
        if (start == 0) { break; }

        memcpy(f1, f2, sizeof(f1));
        memcpy(f2, ftemp, sizeof(f2));
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 addition(int32 sum[MAX_DIGITS], int32 a[MAX_DIGITS], int32 b[MAX_DIGITS])
{
    memset(sum, 0, sizeof(int32) * MAX_DIGITS);

    int32 start_a = find_start_index(a);
    int32 start_b = find_start_index(b);
    int32 start = start_a < start_b ? start_a : start_b;

    for (int32 i = MAX_DIGITS - 1; i >= start; --i)
    {
        sum[i] += a[i] + b[i];
        if (sum[i] >= 10)
        {
            if (i - 1 < 0)
            { return 1; }

            sum[i] -= 10;
            sum[i - 1] += 1;
        }
    }

    return 0;
}

int32 find_start_index(int32 number[MAX_DIGITS])
{
    for (int32 i = 0; i < MAX_DIGITS; ++i)
    {
        if (number[i] != 0)
        {
            return i;
        }
    }

    return MAX_DIGITS - 1;
}
