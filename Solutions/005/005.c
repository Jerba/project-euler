#include <stdio.h>

/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

int main()
{
    printf("Project Euler 005 - Smallest multiple\n"
            "https://projecteuler.net/problem=5");

    long long i = 0;
    while (1)
    {
        i += 20; // 2, 4, 5, 10, 20
        if (i % 19 != 0) { continue; }
        if (i % 18 != 0) { continue; }
        if (i % 17 != 0) { continue; } 
        if (i % 16 != 0) { continue; } 
        if (i % 15 != 0) { continue; } 
        if (i % 14 != 0) { continue; } 
        if (i % 13 != 0) { continue; } 
        if (i % 12 != 0) { continue; } 
        if (i % 11 != 0) { continue; } 

        break;
    }

    printf("\n\nAnswer: %lli", i);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}