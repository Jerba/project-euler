#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define MAX_DIGITS 256
typedef int32_t int32;
typedef uint64_t uint64;

/*
A googol (10^100) is a massive number: one followed by one-hundred zeros; 100^100 is almost unimaginably large: one followed by two-hundred zeros.
Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, a^b, where a, b < 100, what is the maximum digital sum?
*/

int32 multiply(int32 number[MAX_DIGITS], int32 factor);
int32 find_start_index(int32 number[MAX_DIGITS]);
int32 get_num_digits(int32 number);
void init_digits(int32 digits[MAX_DIGITS], int32 number);

int main()
{
    printf("Project Euler 056 - Powerful digit sum\n"
            "https://projecteuler.net/problem=56");

    int32 answer = 0;
    int32 arr[MAX_DIGITS] = {0};

    for (int32 a = 2; a < 100; ++a)
    {
        for (int32 b = 2; b < 100; ++b)
        {
            init_digits(arr, a);

            for (int32 i = b; i > 1; --i)
            { multiply(arr, a); }

            int32 digital_sum = 0;
            for (int32 i = find_start_index(arr); i < MAX_DIGITS; ++i)
            { digital_sum += arr[i]; }

            if (digital_sum > answer)
            { answer = digital_sum; }
        }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 multiply(int32 number[MAX_DIGITS], int32 factor)
{
    if (factor == 1)
    { return 0; }

    if (factor == 0)
    {
        memset(number, 0, sizeof(int32) * MAX_DIGITS);
        return 0;
    }

    if (factor < 0)
    { factor *= -1; }

    int32 original_number[MAX_DIGITS] = {0};
    memcpy(original_number, number, sizeof(int32) * MAX_DIGITS);
    memset(number, 0, sizeof(int32) * MAX_DIGITS);

    int32 i = 0, j = 0, carry = 0;

    int32 start_index   = find_start_index(original_number);
    int32 end_index     = MAX_DIGITS - 1;
    int32 num_digits    = get_num_digits(factor);
    int32 temp          = factor;

    int32 temp_value[MAX_DIGITS] = {0};
    for (i = 0; i < num_digits; ++i)
    {

        int32 digit = temp % 10;
        temp /= 10;

        for (j = end_index; j >= start_index; --j)
        {
            int32 val = (original_number[j] * digit) + carry;

            carry = val / 10;
            val -= 10 * carry;
            temp_value[j] = val;
        }

        int32 index = start_index;

        if (carry > 0)
        {
            if (start_index - 1 < 0) { goto error; }
            --index;

            temp_value[index] = carry;
            carry = 0;
        }

        for (j = end_index; j >= index; --j)
        {
            if (j - i - 1 < 0) { goto error; }

            int32 temp_val = number[j - i] + temp_value[j];
            int32 temp_carry = temp_val / 10;
            temp_val -= 10 * temp_carry;

            number[j - i] = temp_val;
            number[j - i - 1] += temp_carry;
        }
    }

    return 0;

    error:
    memcpy(number, original_number, sizeof(number));
    printf("\n\nError: Number was too big to calculate.");

    return 1;
}

int32 find_start_index(int32 number[MAX_DIGITS])
{
    for (int32 i = 0; i < MAX_DIGITS; ++i)
    {
        if (number[i] != 0)
        {
            return i;
        }
    }

    return MAX_DIGITS - 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}

void init_digits(int32 digits[MAX_DIGITS], int32 number)
{
    if (number < 0) { number *= -1; }

    memset(digits, 0, sizeof(int32) * MAX_DIGITS);

    int32 index = MAX_DIGITS - 1;
    while (number != 0)
    {
        int32 digit = number % 10;
        number /= 10;
        digits[index] = digit;
        index--;
    }
}
