#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define PRIME_CACHE_SIZE 1000000
#define NUM_PRIMES 78498

typedef int32_t int32;

/*
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/

void fill_prime_cache();
int32 is_prime(int32 number);
int32 get_num_digits(int32 number);
int32 get_first_digit(int32 number);

int32 primes[NUM_PRIMES];
int32 prime_cache[PRIME_CACHE_SIZE];

int main()
{
    printf("Project Euler 037 - Truncatable primes\n"
            "https://projecteuler.net/problem=37");

    fill_prime_cache();

    int32 answer = 0;
    for (int32 i = 4; i < NUM_PRIMES; ++i)
    {
        int32 prime = primes[i];
        int32 flag = 1;

        while (prime != 0)
        {
            prime /= 10;
            if (prime > 0 && !prime_cache[prime])
            {
                flag = 0;
                break;
            }
        }

        if (!flag)
        { continue; }

        prime = primes[i];
        int32 num_digits = get_num_digits(prime);

        while (num_digits > 1)
        {
            int32 temp = get_first_digit(prime) * 10;

            while (get_num_digits(temp) < num_digits)
            { temp *= 10; }

            prime -= temp;

            if (!prime_cache[prime])
            {
                flag = 0;
                break;
            }

            num_digits = get_num_digits(prime);
        }

        if (flag)
        { answer += primes[i]; }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

void fill_prime_cache()
{
    int32 num_primes = 0;
    for (int32 i = 0; i < PRIME_CACHE_SIZE; ++i)
    {
        int32 result = is_prime(i);
        prime_cache[i] = result;

        if (result && num_primes < NUM_PRIMES)
        {
            primes[num_primes] = i;
            ++num_primes;
        }
    }
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return 1; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return 0; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 get_num_digits(int32 number)
{
    if (number == 0) { return 1; }
    return (int32)floor(log10(abs(number))) + 1;
}

int32 get_first_digit(int32 number)
{
    if (number == 0) { return 0; }

    int32 first = number;

    if (first < 0)
    { first *= -1; }

    while (first >= 10)
    { first /= 10; }

    return first;
}
