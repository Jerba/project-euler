#include <stdio.h>
#include <math.h>
#define BUFFER_SIZE 512

/*
2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2^1000?
*/

int get_number(char character);

int main()
{
    printf("Project Euler 016 - power digit sum\n"
            "https://projecteuler.net/problem=16");

    int answer = 0;
    double value = pow(2, 1000);

    char buffer[BUFFER_SIZE];
    sprintf(buffer, "%lf\0", value);
    printf("\n\n2^1000: %lf", value);

    for (int i = 0; i < BUFFER_SIZE; ++i)
    {
        int digit = get_number(buffer[i]);
        if (digit == -1)
        { break; }

        answer += digit;
    }

    printf("\n\nAnswer: %i", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int get_number(char character)
{
    int temp = (character - '0');
    if (temp >= 0 && temp <= 9) { return temp; }

    return -1;
}
