#include <stdio.h>
#include <string.h>

#define TRIANGLE_SIZE 15

/*
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

   3
  7 4
 2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

              75
             95 64
            17 47 82
           18 35 87 10
          20 04 82 47 65
         19 01 23 75 03 34
        88 02 77 73 07 63 67
       99 65 04 28 06 16 70 92
      41 41 26 56 83 40 80 70 33
     41 48 72 33 47 32 37 16 94 29
    53 71 44 65 25 43 91 52 97 51 14
   70 11 33 28 77 73 17 78 39 68 17 57
  91 71 52 38 17 14 91 43 58 50 27 29 48
 63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
*/

int triangle[TRIANGLE_SIZE][TRIANGLE_SIZE] =
{
    { 75,                                                        },
    { 95, 64,                                                    },
    { 17, 47, 82,                                                },
    { 18, 35, 87, 10,                                            },
    { 20,  4, 82, 47, 65,                                        },
    { 19,  1, 23, 75,  3, 34,                                    },
    { 88,  2, 77, 73,  7, 63, 67,                                },
    { 99, 65,  4, 28,  6, 16, 70, 92,                            },
    { 41, 41, 26, 56, 83, 40, 80, 70, 33,                        },
    { 41, 48, 72, 33, 47, 32, 37, 16, 94, 29,                    },
    { 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14,                },
    { 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57,            },
    { 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48,        },
    { 63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31,    },
    {  4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23 }
};

int max(int a, int b)
{
    return a > b ? a : b;
}

int get_maximum_path_sum_brute(int row, int col, int sum)
{
    sum += triangle[row][col];
    ++row;

    if (row >= TRIANGLE_SIZE)
    { return sum; }

    int temp1 = get_maximum_path_sum_brute(row, col, sum);
    int temp2 = temp1;

    if (col + 1 < TRIANGLE_SIZE)
    { temp2 = get_maximum_path_sum_brute(row, col + 1, sum); }

    if (temp1 > temp2) { return temp1; }
    return temp2;
}

int get_maximum_path_sum()
{
    int temp_arr[TRIANGLE_SIZE][TRIANGLE_SIZE];
    memcpy(temp_arr, triangle, sizeof(temp_arr));

    for (int row = TRIANGLE_SIZE - 2; row >= 0; --row)
    {
        for (int col = 0; col < TRIANGLE_SIZE - 1; ++col)
        {
            temp_arr[row][col] += max(temp_arr[row + 1][col], temp_arr[row + 1][col + 1]);
        }
    }

    return temp_arr[0][0];
}

int main()
{
    printf("Project Euler 018 - Maximum path sum I\n"
            "https://projecteuler.net/problem=18");

    int answer = get_maximum_path_sum();
    // int answer_alt = get_maximum_path_sum_brute(0, 0, 0);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");
    getchar();

    return 0;
}
