#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

typedef int32_t int32;

/*
Euler discovered the remarkable quadratic formula:

n^2 + n + 41

It turns out that the formula will produce 40 primes for the consecutive integer values 0 ≤ n ≤ 39.
However, when n = 40, 40^2 + 40 + 41 = 40(40+1 )+ 41 is divisible by 41, and certainly when n = 41, 41^2 + 41 + 41 is clearly divisible by 41.

The incredible formula n^2 − 79n + 1601
was discovered, which produces 80 primes for the consecutive values 0 ≤ n ≤ 79.
The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:
    n^2 + an + b,

    where |a| < 1000 and |b| ≤ 1000
    where |n| is the modulus/absolute value of n
    e.g. |11| = 11 and |−4| = 4

Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
*/

int32 hash_primes(int32 num);
int32 is_prime(int32 number);
int32 is_prime_hashed(int32 number);

int32 *primes = 0;
int32 primes_len = 0;

int main()
{
    printf("Project Euler 027 - Quadratic primes\n"
            "https://projecteuler.net/problem=27");

    if (hash_primes(16384) != 0)
    { return 0; }

    int32 answer = 0;
    int32 best_streak = 0;

    for (int32 a = -1000; a < 1000; ++a)
    {
        int32 abs_a = abs(a);
        for (int32 b = -1000; b <= 1000; ++b)
        {
            int32 streak = 0;
            for (int32 n = 0; n <= abs_a; ++n)
            {
                int32 result = n * n + a * n + b;

                int32 prime_result = is_prime_hashed(result);
                if (prime_result == 1)
                {
                    ++streak;
                    if (streak > best_streak)
                    {
                        best_streak = streak;
                        answer = a * b;
                    }
                }
                else if (prime_result == -1)
                {
                    answer = -1;
                    goto cleanup;
                }
                else
                {
                    break;
                }
            }
        }
    }

    cleanup:
    free(primes);

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 hash_primes(int32 num)
{
    if (primes_len > 0 && num <= primes_len)
    {
        return 0;
    }

    if (num <= 0) { return 1; }

    if (primes == 0)
    {
        primes = (int32*)malloc(sizeof(int32) * (num + 1));
    }
    else if (num > primes_len)
    {
        int32 *temp = (int32*)realloc(primes, sizeof(int32) * (num + 1));

        if (temp == 0)
        {
            return 2;
        }

        primes = temp;
    }

    for (int32 i = primes_len; i <= num; ++i)
    {
        primes[i] = is_prime(i);
    }

    primes_len = num;
    return 0;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return 1; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return 0; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 is_prime_hashed(int32 number)
{
    number = abs(number);

    if (hash_primes(number) != 0)
    { return -1; }

    return primes[number];
}