#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define BUFFER_LENGTH 65536
#define MAX_NAMES 65536
#define NAME_LENGTH 32

typedef int32_t int32;
typedef uint64_t uint64;

/*
Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
*/

int32 read_names();
int32 sort_names();
int32 print_names();
int32 get_total_score();

char names[MAX_NAMES][NAME_LENGTH];
int32 num_names = 0;

int main()
{
    printf("Project Euler 022 - Names scores\n"
            "https://projecteuler.net/problem=22");

    if (read_names() != 0)
    { return 0; }

    if (sort_names() != 0)
    { return 0; }

    int32 answer = get_total_score();

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 read_names()
{
    FILE *fp;

    fp = fopen("p022_names.txt", "r");
    if (fp == 0)
    {
        printf("\nCould not open file %s", "p022_names.txt");
        return 1;
    }

    char str[BUFFER_LENGTH];
    char *name;
    fread(str, sizeof(char), BUFFER_LENGTH, fp);

    name = strtok(str, ",");
    while (name != 0)
    {
        int32 i = 0, j = 0;
        for (i = 1; i < NAME_LENGTH - 1; ++i)
        {
            if (name[i] == '\"')
            { break; }

            names[num_names][j] = name[i];
            ++j;
        }

        names[num_names][j] = '\0';
        ++num_names;

        name = strtok(0, ",");
    }

    fclose(fp);
    return 0;
}

int32 sort_names()
{
    if (num_names <= 0)
    { return 1; }

    char temp[NAME_LENGTH] = "\0";
    int32 i, j;

    for (i = 0; i < num_names - 1; ++i)
    {
        for (j = i + 1; j < num_names; ++j)
        {
            if (strcmp(names[i], names[j]) > 0)
            {
                strcpy(temp, names[i]);
                strcpy(names[i], names[j]);
                strcpy(names[j], temp);
            }
        }
    }

    return 0;
}

int32 print_names()
{
    if (num_names <= 0)
    { return 1; }

    printf("\n\nNames:\n");
    for (int32 i = 0; i < num_names; ++i)
    {
        printf("%s\n", names[i]);
    }

    return 0;
}

int32 get_total_score()
{
    if (num_names <= 0)
    { return 0; }

    int32 total_score = 0;
    int32 name_score = 0;

    int32 i, j;
    for (i = 0; i < num_names; ++i)
    {
        name_score = 0;

        for (j = 0; j < strlen(names[i]); ++j)
        {
            char c = names[i][j];
            name_score += (c >= 'A' && c <= 'Z') ? (c - 'A' + 1) : 0;
        }

        name_score *= (i + 1);
        total_score += name_score;
    }

    return total_score;
}
