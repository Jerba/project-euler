#include <stdio.h>
#include <stdint.h>

typedef int32_t int32;

/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
*/

int32 is_prime(int32 number);
int32 is_pandigital_one_to_n(int32 number);

int main()
{
    printf("Project Euler 041 - Pandigital prime\n"
            "https://projecteuler.net/problem=41");

    int32 answer = 0;
    for (int32 i = 1234567; i <= 7654321; i += 2)
    {
        if (is_pandigital_one_to_n(i) && is_prime(i))
        { answer = i; }
    }

    printf("\n\nAnswer: %d", answer);
    printf("\n\nPress Any Key to Quit\n");  
    getchar();

    return 0;
}

int32 is_prime(int32 number)
{
    if (number < 0) { number *= -1; }
    if (number == 2 || number == 3 || number == 5) { return 1; }
    if (number <= 1 || number % 2 == 0 || number % 3 == 0) { return 0; }

    int i = 5;
    while (i * i <= number)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        { return 0; }

        i += 6;
    }

    return 1;
}

int32 is_pandigital_one_to_n(int32 number)
{
    if (number == 0) { return 0; }

    int32 used_digits[10] = {0};
    int32 high = 0;

    while (number != 0)
    {
        int32 digit = number % 10;
        number /= 10;

        if (digit == 0)
        { return 0; }

        used_digits[digit] += 1;
        if (used_digits[digit] == 2)
        { return 0; }

        if (digit > high)
        { high = digit; }
    }

    for (int32 i = 1; i <= high; ++i)
    {
        if (used_digits[i] == 0)
        { return 0; }
    }

    return 1;
}
